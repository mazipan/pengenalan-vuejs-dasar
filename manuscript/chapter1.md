# Bagian 1 - Perkenalan dengan Vue.js

Pada bagian ini, kita akan mengenal dan belajar berbagai hal mengenai dasar mengenai Vue.js mulai dari proses instalasi, pengenalan dasar komponen, *single file component* (SFC) yang biasa dipakai di projek Vue.js, sampai pengetahuan dasar data-binding di Vue.js.

Mari kita mulai perjalanan kita berkenalan dengan Vue.js.

## 1.1 Mengenal Vue.js

![Logo Vue.js](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Vue.png/215px-Vue.png)

Vue.js ([https://vuejs.org/](https://vuejs.org/)) merupakan framework JavaScript besutan [Evan You](https://twitter.com/youyuxi) yang membantu kita dalam membuat sebuah website/aplikasi yang membutuhkan banyak interaksi di dalamnya,
biasanya berupa [Single Page Application](https://en.wikipedia.org/wiki/Single-page_application) (SPA) meskipun pada dasarnya Vue.js tidak hanya bisa digunakan untuk website yang mengadopsi SPA.
Belakangan ini Vue.js sedang naik daun popularitasnya dikalangan para *frontend developer*.
Naik daunnya Vue tentu tidak serta merta begitu saja, namun memang diiringi dengan kemudahan adopsi, dokumentasi yang lengkap dan mudah dipahami, namun dengan performa yang masih sangat layak untuk dibandingkan dengan para kompetitornya.

Evan You sendiri merupakan mantan karyawan Google yang sempat intens dalam mengembangkan framework AngularJS pada awalnya, sehingga bisa dimaklumi bahwa Vue.js sangat terinspirasi dari AngularJS. Ada banyak konsep dan sintaks yang mirip dengan yang ada di AngularJS. Itu pula yang membuat menjadi mudah bagi para developer yang sudah lekat dengan sintaks AngularJS untuk berpindah dan belajar Vue.js. Vue.js juga banyak mengadopsi konsep dari React.js dan berbagai lingkungan pendukungnya. Wajar juga mengingat popularitas dan kematangan React.js selama ini, Vue.js sebagai yang lebih muda bisa disebut juga sebagai *fusion* dari AngularJS dan React.js. Mengambil banyak keuntungan dan kemudahan dari AngularJS dan ekspresif serta modernnya sintaks React.js serta lingkungan disekitarnya.

## 1.2 Inisialisasi Projek

Vue.js adalah *framework* yang bisa berdiri sebagai *full feature framework* maupun *library* atau pustaka yang bisa dikombinasikan dengan berbagai *framework* atau *library* lain, sehingga untuk proses instalasi pun tergolong mudah dan bisa dilakukan langsung lewat peramban (*browser*) seperti ketika kita ingin menggunakan *library* legenda [jQuery](https://jquery.com/) namun kita juga bisa memasangnya dengan lebih modern dan melewati proses *bundling* layaknya [React](https://reactjs.org/) dan [Angular](https://angular.io/) versi terbarunya.

## 1.2.1 Inisialisasi di Browser

Berikut adalah cara paling sederhana untuk menggunakan Vue.js di dalam aplikasi kita, yakni dengan secara langsung menambahkan eksternal script kedalam html kita, seperti:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Panduan Dasar Vue.js</title>
  </head>
  <body>
    <div id="app">
      {{ message }}
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
    const app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue!'
      }
    });
    </script>
  </body>
</html>
```

Dengan kode sederhana tersebut, kita sudah berhasil menginisialisi Vue.js di dalam projek web kita.
Kode tersebut akan menampilkan pesan sederhana **“Hello Vue!”** seperti yang kita definisikan di dalam data `message`.

Contoh kode ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/PaGzGv](https://codepen.io/mazipan/pen/PaGzGv)

## 1.2.2 Inisialisasi dengan Vue-CLI

**Vue-CLI** (https://cli.vuejs.org/) merupakan alat yang dibuat oleh tim pengembang dari Vue.js dan ditujukan untuk mempermudah pengembangan dan pembuatan struktur projek Vue.js.
Seperti [Angular-CLI](https://cli.angular.io/) pada Angular framework dan [Create React App](https://github.com/facebook/create-react-app) pada React.js, Vue-CLI memiliki peran yang hampir sama.

Vue-CLI pada saat saya menulis buku ini tengah mencapai versi 3 yang mana masih dalam tahap pengembangan dan masih beta, tapi saya akan coba memberikan contoh dari kedua versi baik sebelum versi 3 maupun setelah versi 3 ini dibuat.

Sebelum memulai menggunakan Vue-CLI, ada beberapa hal yang harus sudah ada di komputer kalian yakni:

1.	Node.js dan NPM (versi LTS terbaru) - https://nodejs.org/en/download/
1.	GIT - https://git-scm.com/downloads
1.	Koneksi Internet yang stabil

### 1.2.2.1 Inisialisasi Vue-CLI v2.9.2

Memasang Vue-CLI versi 2.9.2 di komputer kalian bisa dengan menggunakan NPM dengan cara mengetikkan perintah:

```bash
npm install -g vue-cli@2.9.2
# atau dengan Yarn
yarn global add vue-cli@2.9.2
```

Setelah proses memasang Vue-CLI telah selesai, kalian seharusnya bisa mengecek versi yang terpasang di computer kalian dengan menjalankan perintah `vue --version` di *command prompt* (CMD).

Menggunakan Vue-CLI versi 2, kita bisa memilih template yang akan kita gunakan untuk projek kita yang bisa disesuaikan dengan kebutuhan.
Template ini sendiri dikelola langsung oleh team dari Vue.js yang bisa kalian temui di github [https://github.com/vuejs-templates/](https://github.com/vuejs-templates/).
Untuk buku ini kita akan memilih template dari webpack, dimana kita bisa memasang dan menggunakan template dari webpack ini dengan cara menjalankan perintah berikut di CMD kalian:

```bash
vue init webpack my-project
```

Setelah menjalankan proses ini, akan ada beberapa pertanyaan terkait dengan projek yang ingin dibuat seperti nama projek, deskripsi, nama pembuat, ingin menggunakan *ESLint* atau tidak? Ingin menambahakan *Vue-Router* atau tidak? Apakah ingin menambahkan *CSS Pre-processor* atau tidak? Ingin melakukan konfigurasi *Unit Testing* dan *E2E Test* atau tidak? Semua pertanyaan ini bisa kalian jawab tergantung kebutuhan masing-masing.
Untuk pertama kali saya sarankan untuk mengambil hal-hal yang sederhana saja, tidak perlu sampai setup *ESlint*, *Unit Test* apalagi *E2E Test*.

Jika proses ini telah selesai, kita bisa masuk proses berikutnya yakni mengunduh semua *dependencies* yang dibutuhkan oleh Vue dan menjalankan di local untuk pertama kalinya.
Untuk melakukan ini semua, kita perlu masuk ke folder project kita terlebih dahulu dengan perintah `cd my-project`, kemudian menjalankan `npm install` untuk mengunduh semua paket *dependencies*.
Setelah selesai kita bisa mulai menjalankan di komputer lokal dengan perintah `npm run dev`.


### 1.2.2.2 Inisialisasi Vue-CLI v3 (beta)

Untuk memasang Vue-CLI v3 ini kita bisa menggunakan perintah:

```bash
npm install -g @vue/cli
# atau dengan Yarn
yarn global add @vue/cli
```

Setelahnya, kita bisa menjalankan perintah untuk membuat projek Vue baru dengan mengetikkan perinta berikut:

```bash
vue create my-project
```

Dalam membuat projek baru menggunakan Vue CLI v3 kita bisa memilih dua macam konfigurasi yakni standard dan manual. Pilihan konfigurasi standard bisa kita pilih bila kita mengharapkan konfigurasi minimal tanpa banyak hal lain lagi yang harus kita tambahkan kedalam projek kita. Contoh pemilihan konfigurasi standard bisa dilihat pada gambar berikut:

![Pilihan preset standar](https://cli.vuejs.org/cli-new-project.png)

Sedangkan pilihan manual bisa memberikan kita lebih banyak kemampuan untuk menambahkan berbagai kebutuhan yang lebih lanjut seperti menambahkan *Vue Router*, *Vuex*, *CSS Pre-processor*, *Unit Testing* dan juga *End-to-End Testing*. Gambar berikut merupakan ilustrasi saat kita memilih konfigurasi manual:

![Pilihan preset manual](https://cli.vuejs.org/cli-select-features.png)

Vue CLI v3 ini juga bisa digunakan lewat tampilan UI atau biasa dikenal dengan Vue UI, seperti terlihat pada gambar berikut:

![Vue UI](https://cli.vuejs.org/ui-new-project.png)

Untuk contoh penggunaan bisa kita lihat di YouTube channel Mas Mulia Nasution di tautan berikut
[https://vuejs.id/video-tutorial/UwMEsbVNiI0.html](https://vuejs.id/video-tutorial/UwMEsbVNiI0.html)




## 1.3 Inisialisasi tanpa Vue-CLI

Kalian mungkin seperti terjun ke dunia asing yang bukan JavaScript setelah membaca dan mencoba konfigurasi projek Vue, tapi kalau mau kembali ke dasar sebenarnya Vue ini sama saja dengan JavaScript pada umumnya. Yang membedakan hanya pada file `.vue` yang digunakan oleh Vue ini bukanlah file JavaScript biasa yang bisa diproses oleh browser. File ini harus diterjemahkan kedalam file HTML, CSS dan JavaScript biasa sebelum kita mengirimkannya ke Browser. Pun ketika mencoba menginisialisai sebuah projek kita sebenarnya telah dimudahkan dengan kehadiran Vue-CLI yang akan membuatkan *skeleton* projek Vue yang standard dan jamak digunakan oleh banyak orang. Namun untuk mengetahui apa yang sebenarnya Vue-CLI lakukan dibelakangnya maka ada baiknya kita juga belajar untuk melakukan inisialisai tanpa bantuan Vue-CLI sehingga tidak perlu lagi bingung dengan Vue-CLI ketika pada projek sesungguhnya perlu melakukan berbagai perubahan pada konfigurasinya.

Berikut cara melakukan inisialisasi project Vue tanpa Vue-CLI:

// TODO 


## 1.3 Pengenalan Komponen

[Komponen](https://vuejs.org/v2/guide/components.html) merupakan bagian yang sangat sering akan kita temui ketika melakukan pengembangan web menggunakan framework Vue.js.
Komponen bisa dijelaskan secara sederhana merupakan bagian kecil dari sebuah *building block* yang berdiri secara independen, sehingga biasanya komponen ini memiliki CSS stylesheet dan JS script yang terisolasi dari komponen lain.

Berikut cara sederhana mendefinisikan komponen di dalam Vue.js:

Pertama kita akan mendefinisikan sebuah Vue *instance* baru yang akan kita tempel ke element dengan id `#app`,
kemudian kita juga akan menambahkan sebuah inisialisasi komponen dengan nama `button-counter` yang berisikan html element button.

```javascript
Vue.component('button-counter', {
  data: function () {
    return {
      count: 0
    }
  },
  template: `
  <button v-on:click="count++">
    You clicked me {{ count }} times.
  </button>`
});

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});
```

Setelah mendefinisikan komponen di JavaScript, maka langkah kedua kita akan membuat HTML template yang akan menampilkan komponen yang telah kita buat sebelumnya.
Hal menarik dengan komponen ini adalah bahwa kita bisa memanggil dengan namanya saja, tanpa perlu lagi menuliskan template HTMLnya lagi.
Jadi dengan demikian HTML kita terlihat lebih rapi dan ramping.
Dengan komponen `button-counter` yang sudah kita buat tadi, kita cukup membuat tag `<button-counter></button-counter>` untuk memanggil komponen tersebut.

```html
<div id="app">
  {{ message }}
  <br>
  <button-counter></button-counter>
</div>
```

Kode lengkap dari contoh pembuatan komponen sederhana ini bisa kalian lihat di tautan berikut:

[https://codepen.io/mazipan/pen/KeMWme](https://codepen.io/mazipan/pen/KeMWme)

## 1.4 Single File Component

Bila pada bagian sebelumnya kita belajar membuat komponen secara sederhana dan bisa langsung di jalankan di browser tanpa perlu proses kompilasi, maka kali ini kita akan melihat file dengan ekstensi `.vue` atau biasa disebut [Single File Component (SFC)](https://vuejs.org/v2/guide/single-file-components.html).
File ini merupakan file komponen yang independen dimana HTML template, JS Script dan CSS style bisa digabungkan didalam satu file yang memudahkan dalam organisasi struktur projek ketika sudah semakin besar.

```html
<template>
  <div class="app">
    {{ message }}
  <div>
</template>

<script>
export default {
  data: function () {
    return {
      message: 'Hello Vue!'
    }
  }
}
</script>

<style scoped>
.app {
  font-size: 16px;
  text-align: center;
}
</style>
```

Kode diatas merupakan contoh SFC, bisa dilihat ada tiga tag utama di dalam file tersebut yakni `<template></template>` yang berisi HTML template dari komponen yang akan kita buat, `<script></script>` yang akan berisi berbagai script komponen Vue.js, dan `<style></style>` yang berisi berbagai *stylesheet* untuk file komponen tersebut.
Sedikit fitur dalam tag style kita bisa menambahkan `scoped` bila ingin menggunakan style yang diisolasi dalam suatu komponen.
Kita juga bisa dengan mudah menambahkan `lang=scss` bila ingin menggunakan pre-processor SASS misalnya atau `lang=less` bila lebih memilih LESS.
Menggunakan file `.vue` tidak bisa langsung di browser, karena browser pada dasarnya tidak mengenal jenis file ini, itu kenapa kita butuh bantuan [webpack](https://webpack.js.org/) atau lainnya untuk melakukan kompilasi menjadi file JS dan CSS biasa.
Untuk konfigurasi webpack ini kita tidak akan jelaskan pada buku ini, namun kita cukup menggunakan Vue-CLI yang sudah dijelaskan pada bagian sebelumnya saja.

## 1.5 Data Binding

**Data Binding** merupakan hal dasar yang harus kita tahu ketika menggunakan framework JavaScript modern seperti Vue.js dan lainnya. Data Binding ini pada dasarnya memiliki tujuan yang sama di semua framework, yakni menghindarkan kita dari melakukan manipulasi DOM secara langsung dan cukup mengubah bagian tertentu dan manipulasi DOM akan ditangani oleh framework tersebut.

Data Binding paling tidak ada dua jenis *one-way* dan *two-way*.
**One-way** data binding berarti kita bisa membaca data dari sebuah JavaScript sintaks dan akan langsung terlihat nilai dari data tersebut di DOM yang kita lihat di peramban.
Kenapa disebut *one-way* karena memang kemampuannya yang cuma sebatas membaca nilai dari data tanpa bisa melakukan perubahan terhadap nilai itu secara langsung dari DOM. Sementara *two-way* berlaku sebaliknya, *two-way* data binding memiliki kemampuan membaca sekaligus mengubah nilai dari data tersebut secara langsung lewat DOM.

## 1.5.1 Binding View

Vue.js menggunakan sintaks dua kurung kurawal `{{ }}` untuk melakukan interpolasi data atau binding data kedalam view, seperti terlihat pada gambar berikut.
Dengan sintaks tersebut berarti kita ingin menampilkan sebuah data yang ada dalam Javascript kita ke dalam HTML template.
Sintaks ini merupakan *one-way* data binding dan digunakan hanya untuk menampilkan data, tanpa adanya kemampuan untuk mengubah nilai data tersebut.

Berikut contoh implementasi data binding ke view HTML:

```html
<div id="app">
  {{ message }}
  <br>
  <message></message>
</div>
```

```javascript
Vue.component('message', {
  data: function () {
    return {
      message2: 'Hello again from component!'
    }
  },
  template: '<div>{{ message2 }}</div>'
});

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});
```

Contoh kode berkaitan dengan binding kedalam view bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/gKMmGW](https://codepen.io/mazipan/pen/gKMmGW)

## 1.5.2 Binding Attribute

Di Vue.js kita menggunakan `v-bind` untuk melakukan binding ke dalam HTML.
Jika kita melihat ke gambar diatas berarti kita ingin menambahkan attribute title ke dalam span dengan menggunakan data dinamis dari javascript. 
`v-bind` pada dasarnya bisa diterapkan pada berbagai attribute di HTML sehingga kita mungkin akan menemui banyak variasi `v-bind` ini seperti `v-bind:src`, `v-bind:class`, `v-bind:alt`, dan lain sebagainya. 

Vue.js juga menyediakan cara yang lebih cepat untuk kita mendefinisikan v-bind di dalam HTML yakni dengan menghilangkan bagian `v-bind`nya, jadi kita bisa menggunakan sintask seperti ini `:title`, `:src`, `:class`, `:alt` dan sebagainya.

Berikut ini contoh dari implementasi binding data ke attribute:

```html
<div id="app">
  <img :src="image.url"
       :alt="image.alt"
       :height="image.height"
       :width="image.width"/>
  <br/>
  Hover Me, Please!
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    image: {
      url: 'http://placekitten.com/200/200',
      alt: 'Hello I am Kitten',
      height: 200,
      width: 200
    }
  }
});
```

Contoh kode berkaitan dengan binding kedalam attribute bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/eKzWLd](https://codepen.io/mazipan/pen/eKzWLd)

## 1.5.3 Binding Events

Untuk memanggil suatu event yang telah kita buat di Javascript,
Vue.js menggunakan `v-on` di dalam HTML template diikuti hook event yang akan ditambahkan.
Jadi kita bisa menggunakan berbagai variasi `v-on` seperti `v-on:click`, `v-on:blur`, `v-on:focus`, `v-on:keyup`, `v-on:mouseover` dan sebagainya.

Vue.js menyediakan juga cara lebih cepat untuk menuliskan event binding ini yakni menggunakan `@`, sehingga kita bisa menyingkat seperti `@click`, `@blur`, `@focus`, `@keyup`, `@mouseover` dan lain sebagainya.

Berikut contoh dari implementasi binding events:

```html
<div id="app">
  <button v-on:click="clickMe('Button 1')">
    Click Me!
  </button>
  <br/><br/>
  <img src="http://placekitten.com/100/100"
       v-on:mouseover="hoverMe('Image')" />
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  methods: {
    clickMe: function (message) {
      alert('Hey, you click me: ' + message)
    },
    hoverMe: function (message) {
      alert('Hey, you hover me: ' + message)
    }
  }
});
```

Contoh kode berkaitan dengan binding event bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/wXWdNL](https://codepen.io/mazipan/pen/wXWdNL)

## 1.5.4 Two-way Data Binding

Vue.js juga menyediakan fitur *two-way* data binding yang artinya setiap perubahan pada Javascript akan berpengaruh pada view HTML dan sebaliknya perubahan pada HTML view pun akan mempengaruhi atau mengubah nilai di Javascript nya.

Di Vue.js kita menggunakan `v-model` untuk melakukan *two-way* data binding yang biasanya sering ditempelkan pada sebuah elemen input HTML, seperti pada kode berikut:

```html
<div id="app">
  <input type="text"
        v-model="message"/>
  {{ message }}

  <br/><br/>
  <input type="checkbox"
         v-model="checked"
         id="checkbox"/>
  <label for="checkbox">
    {{ checked }}
  </label>

  <br/><br/>
  <select v-model="selected">
    <option disabled value="">
      Please select one
    </option>
    <option>A</option>
    <option>B</option>
    <option>C</option>
  </select>
  <span>
    Selected: {{ selected }}
  </span>

</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    checked: true,
    selected: ''
  }
});
```

Contoh kode berkaitan dengan *two-way* data binding kedalam bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/ZROybK](https://codepen.io/mazipan/pen/ZROybK)

## 1.6 Reactivity di Vue.js

Vue.js adalah salah satu *framework* yang bekerja secara *reactive* terhadap berbagai perubahan yang terjadi pada `object` yang didefinisikan di dalam sistem Vue. Hal ini tentu memudahkan kita karena kita hanya perlu mengubah suatu nilai dan kesemua efeknya akan ditangani oleh Vue secara *reactive* sehingga kita tidak perlu lagi mengubah manual nilai lain yang memiliki korelasi atau memiliki *trigger* untuk melakukan sebuah aksi ketika perubahan itu terjadi.

Pada saat kita melempar suatu nilai pada `data` di Vue, maka otomatis akan dibuatkan `getter` dan `setter` yang masing-masing nilai ini akan berkorelasi pada suatu instance `watcher` yang bekerja memantau setiap perubahan yang terjadi. Setiap kali `setter` terdeteksi terpanggil, maka akan diberitahukan pada `watcher` untuk melakukan perubahan nilai yang berakibat pula dilakukannya *render* ulang pada setiap komponen atau tampilan yang berasosiasi dengan nilai tersebut.

![Ilustrasi reactivity di Vue.js](https://vuejs.org/images/data.png)

Bacaan mengenai *reactivity* di dalam Vue.js ini bisa menjadi topik yang lumayan dasar untuk dipahami, silahkan kunjungi situs resminya untuk memahami lebih lanjut.
Kunjungi [https://vuejs.org/v2/guide/reactivity.html](https://vuejs.org/v2/guide/reactivity.html)

-------

> Objectif: Setelah membaca bagian ini diharapkan pembaca sudah bisa melakukan konfigurasi untuk menyiapkan dan praktek langsung serta mencoba berbagai kode dasar dari Vue yang akan lebih banyak lagi kita tampilkan pada bagian berikutnya.