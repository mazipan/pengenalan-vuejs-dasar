# Bagian 0 - Pembukaan

## 0.1 Kata Pengantar

Syukur Alhamdulillah saya panjatkan kepada Allah yang Maha Kuasa, karena hanya atas berkat rahmat dan riho-Nya lah buku yang berjudul "**Panduan Dasar Vue.js**" akhirnya dapat saya selesaikan dan publikasikan.

Dalam pembuatan buku ini, ucapan terima kasih saya sampaikan kepada semua teman-teman yang mendukung dari awal sampai selesai. Juga kepada teman-teman yang telah dengan ikhlas meluangkan sebagian uang dan waktu untuk membeli dan membaca buku ini. Segala dukungan ini selalu menjadi penyemangat saya untuk terus berkarya dan memberikan manfaat bagi teman-teman.

Ucapan terima kasih tak lupa saya sampaikan kepada segenap keluarga saya tercinta, nenek, orang tua, adik-adik, istri, dan anak-anak serta semua keluarga besar saya yang senantiasa mendukung berbagai aktivitas saya termasuk dalam pembuatan buku ini.

Demikian, buku ini saya buat dan publikasikan dengan segala kekurangan yang ada karena kesempurnaan memang hanya milik-Nya. Kritik dan saran dari semua pembaca sangat saya harapkan, agar kedepannya buku ini bisa terus menjadi lebih baik dan bisa menjadi referensi belajar yang pas untuk para pengembang web di Indonesia.

Semoga buku ini dapat memberikan manfaat dan pengetahuan bagi para pembaca.

-----------------------------------------------

Jakarta, 02 Juli 2018

Penulis,

Irfan Maulana
