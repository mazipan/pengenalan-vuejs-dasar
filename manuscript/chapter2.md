# Bagian 2 - Pengetahuan Dasar Vue.js

Pada bagian ini, kita sudah mulai memasuki materi untuk mulai memahami berbagai fitur dasar yang ada di Vue.js. Kita akan belajar apa itu *data*, *methods*, *props*, *watch*, *computed*, *directives*, *plugins* sampai *mixins*. Semua penjelasan disertai contoh kode dengan kasus yang diharapkan bisa mempertajam pemahaman kita terhadap hal yang dijelaskan menggunakan kata-kata. Contoh kode juga tersedia secara online di *codepen*, sehingga para pembaca bisa langsung bereksplorasi dengan membaca, memahami dan mengubah kode tersebut agar bisa lebih paham.

Mari kita lanjutkan perjalanan kita berkenalan lebih dalam Vue.js.

## 2.1 Data, Methods dan Props

## 2.1.1 Data

Dari awal kita sebenarnya sudah banyak melihat data ini digunakan di beberapa contoh awal. Sayangnya mungkin belum diberikan penjelasan yang cukup baik.
Data di dalam Vue.js merupakan sekumpulan variabel yang dapat digunakan oleh html template dan dapat dimanipulasi atau dimanfaatkan oleh internal komponen tersebut dan tidak dapat diakses oleh komponen lainnya.
Jadi sebuah variabel di dalam suatu komponen Vue.js hanya akan berpengaruh terhadap komponen itu sendiri.
Data ini mungkin akan lebih familiar bila kita samakan dengan *state* yang ada di *React* dan *framework* JavaScript modern lainnya.

Data sendiri bisa didefinisikan menggunakan sintaks seperti yang terlihat pada potongan kode berikut:

```html
<div id="app">
  {{ message }}
  <br>
  <access-data></access-data>
</div>
```

```javascript
Vue.component('access-data', {
  template: '<div><button @click="clickMe">Click Me!</button></div>',
  data: function () {
    return {
      message2: 'Hello from component!'
    }
  },
  methods: {
    clickMe: function () {
      alert('access the data: ' + this.message2)
    }
  }
});

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});
```

Seperti bisa dilihat pada contoh kode pada gambar diatas, bahwa pendefinisian data menggunakan object jika merupakan Vue *instance* (`new Vue()`).
Dan menggunakan function yang mengembalikan sebuah object jika merupakan extend dari Vue seperti komponen contohnya.
Data bisa langsung diakses di dalam komponen tersebut dengan menggunakan referensi this atau bisa juga dengan menggunakan *instance* dari Vue tersebut denga `app.$data.contohNamaData` dengan app dianggap sebagai Vue *instance*.

Kode untuk mempelajari Data, bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/oyLwwJ](https://codepen.io/mazipan/pen/oyLwwJ)

## 2.1.2 Methods

Methods merupakan opsi dalam Vue.js yang berupa object berisi *function*. *Function* ini sendiri akan memiliki berbagai tujuan yang berbeda-beda dan bervariasi baik memanipulasi data ataupun melakukan sebuah logika bisnis sebuah aplikasi.
*Function* di dalam methods juga bisa menjadi sebuah event yang bisa dipanggil dengan mudah di bagian html template dari komponen yang bersangkutan.
*Function* bisa merupakan sebuah method yang mengembalikan suatu nilai maupun berupa void yang tidak mengembalikan nilai apapun.

Pembuatan methods bisa dilihat dalam contoh kode berikut:

```html
<div id="app">
  {{ count }}
  <br/>
  <button @click="plusOne">Plus One</button>
  <button @click="minOne">Min One</button>
  <button @click="autoAdd">Auto Add</button>
  <button @click="stopAutoAdd">Stop Auto Add</button>
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    count: 0,
    interval: null
  },
  methods: {
    plusOne: function () {
      this.count += 1
    },
    minOne: function () {
      this.count -= 1
    },
    autoAdd: function () {
      let _self = this
      clearInterval(_self.interval)
      _self.interval = setInterval(function () {
        _self.count += 1
      }, 500)
    },
    stopAutoAdd: function () {
      clearInterval(this.interval)
    }
  }
});
```

Kode contoh untuk mempelajari methods bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/YvWQMz](https://codepen.io/mazipan/pen/YvWQMz)

## 2.1.3 Props

Props merupakan parameter yang bisa dilempar oleh sebuah komponen kepada komponen lainnya.
Props merupakan satu dari beberapa jalan untuk mendistribusikan data dari satu komponen ke komponen lainnya. Jadi Props di suatu komponen akan memiliki nilai yang sama dengan nilai dari data yang dilemparkan oleh komponen parent-nya.

Props bisa dibuat dengan cara yang sederhana dengan hanya menyebutkan nama props yang diinginkan, namun bisa juga dibuat dengan lebih komplit dengan menyebutkan berbagai properti-properti lain, seperti tipe data yang diinginkan, default value ketika nilai props tidak didapatkan, dan lain sebagainya.

Kode dibawah merupakan contoh pembuatan Props dengan beberapa variasinya, sebagai berikut:

```html
<div id="app">
  <simple-props
       props1="hey i am props 1"
       :props2="message">
  </simple-props>
  <br/>
  <complex-props
       props1="complex props, i'm props1">
  </complex-props>
</div>
```

```javascript
Vue.component('simple-props', {
  props: ['props1', 'props2'],
  template: '<div>{{ props1 }} <br/> {{ props2 }}</div>'
});

Vue.component('complex-props', {
  props: {
    props1: {
      type: String,
      default: 'why you not fill me?'
    },
    props2: {
      type: String,
      default: 'why you not fill me?',
      required: false
    }
  },
  template: '<div>{{ props1 }} <br/> {{ props2 }}</div>'
});

const app = new Vue({
  el: '#app',
  data: {
    message: 'hey i am message'
  }
});
```

Kode untuk mempelajari props bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/XYKgwP](https://codepen.io/mazipan/pen/XYKgwP)

## 2.2 Watch dan Computed

Vue.js adalah salah satu framework JavaScript yang reactive dari sananya.
Itu kenapa banyak fitur dari Vue.js yang mendukung untuk melakukan berbagai hal reactive, seperti yang akan kita bahas berikut yakni `watch` dan `computed`.

## 2.2.1 Watch

Watch merupakan fitur dari Vue.js yang digunakan untuk memantau suatu perubahan yang terjadi pada sebuah variabel. Namun perlu diketahui bahwa watch akan berjalan dengan baik jika variabel yang dipantau dalam bentuk reactive pula, seperti data ataupun computed (akan dibahas setelah ini).
Dengan menggunakan watch, kita bisa menjadwalkan sebuah perkerjaan atau task untuk dilakukan ketika sebuah perubahan pada variabel terjadi.

Berikut contoh kasus kecil dimana penggunaan watch bisa jadi dibutuhkan:

```html
<div id="app">
  <input type="text"
         placeholder="type me!"
         v-model="message"/>
  <br/><br/>
  Text: {{ message }}
  <br/>
  UpperCase:
  <span style="color:red">{{ upperCaseText }}</span>
  <br/>
  LowerCase:
  <span style="color:green">{{ lowerCaseText }}</span>

</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    message: '',
    upperCaseText: '',
    lowerCaseText: ''
  },
  watch: {
    message: function (newValue, oldValue) {
      this.upperCaseText = newValue.toUpperCase()
      this.lowerCaseText = newValue.toLowerCase()
    }
  }
});
```

Contoh kode untuk mempelajari mengenai watch ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/mKEMYB](https://codepen.io/mazipan/pen/mKEMYB)

## 2.2.2 Computed

Computed memiliki fungsi yang hampir sama dengan watch yakni untuk memantau suatu perubahan terhadap variabel.
Jika pada watch kita memantau perubahan variabel untuk kemudian melakukan suatu task pada saat perubahan itu terjadi, maka pada computed kita akan melakukan suatu task yang akan menghasilkan data baru bilamana salah satu variabel yang berada didalam computed ini mengalami perubahan.

Perbedaan jelas mengenai watch dan computed adalah pada nilai balik, dimana watch tidak harus mengembalikan nilai sementara computed harus mengembalikan nilai yang akan dianggap sebagai data baru.
Hasil dari computed bisa digunakan seperti kita menggunakan data pada umumnya.

Berikut contoh pengguaan computed dengan kasus yang sama dengan yang digunakan pada bagian watch sebelumnya:

```html
<div id="app">
  <input type="text"
         placeholder="type me!"
         v-model="message"/>
  <br/><br/>
  Text: {{ message }}
  <br/>
  UpperCase:
  <span style="color:red">{{ upperCaseText }}</span>
  <br/>
  LowerCase:
  <span style="color:green">{{ lowerCaseText }}</span>
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    message: ''
  },
  computed: {
    upperCaseText: function () {
      return this.message.toUpperCase()
    },
    lowerCaseText: function () {
      return this.message.toLowerCase()
    }
  }
});
```

Contoh kode untuk mempelajari mengenai computed ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/oyLGMp](https://codepen.io/mazipan/pen/oyLGMp)

## 2.3 Directives

Vue.js memang di beberapa bagian mengikuti bagaimana cara AngularJS bekerja, termasuk pada penggunaan directives.
Directives merupakan special attribute pada element HTML yang digunakan oleh Vue.js yang kesemuanya akan menggunakan prefix `v-`.

## 2.3.1 Built-In Directives

Built-In Directive berarti adalah directive yang bisa kita gunakan secara langsung hanya dengan menambahkan vue.js core script, yang artinya semua directive-directive ini memang sudah bawaan dari Vue dan menjadi bagian dari fitur yang Vue.js tawarkan secara default.
Beberapa built-in directive yang akan kita bahas berikut akan sangat berguna dan sering kita gunakan karena memang fungsinya yang umum.

Berikut beberapa built-in directive yang sudah tersedia secara langsung di Vue.js:

### 2.3.1.1 Conditional

Untuk membuat sebuah conditional di HTML template Vue.js, kita bisa menggunakan beberapa directive seperti: `v-if`, `v-else-if` maupun `v-else`.
Dengan menggunakan `v-if` maka kita akan menghilangkan bagian yang tidak terpenuhi kodisinya dari DOM yang di-render oleh browser.

Berikut contoh menggunakan conditional di Vue.js:

```html
<div id="app">
  <select v-model="type">
    <option disabled value="">Please select one</option>
    <option>A</option>
    <option>B</option>
    <option>C</option>
  </select>

  <br/><br/>
  Result:
  <span v-if="type === 'A'">
    A for Apple
  </span>
  <span v-else-if="type === 'B'">
    B for Berry
  </span>
  <span v-else-if="type === 'C'">
    C for Cherry
  </span>
  <span v-else>
    Not A/B/C ? Hmmmm
  </span>
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    type: ''
  }
});
```

Kode contoh untuk memperlajari conditional directive ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/xzOzwO](https://codepen.io/mazipan/pen/xzOzwO)

### 2.3.1.2 Looping

Untuk melakukan looping atau perulangan dari sebuah array, kita bisa menggunakan directive `v-for`.
Looping directive menggunakan `v-for` merupakan hal yang akan sering kita lakukan ketika harus menampilkan element HTML yang berulang dan memiliki kesamaan.
Ada beberapa varian sintaks dari `v-for`, tapi yang berikut akan saya tampilkan adalah mode yang lumayan lengkap beserta `index` dari item yang diulang, dimana ini tidak selalu kita butuhkan setiap kali membuat sebuah perulangan.
Untuk membuat `v-for` kita butuh mendefinisikan `key` dari element tersebut, biasanya diambil dari data yang unik dan tidak ada duplikasi di semua element.

Berikut contoh kode melakukan looping dari sebuah array di Vue.js:

```html
<div id="app">
  <h3>{{ title }}</h3>
  <ul>
    <li v-for="(film, idx) in films"
        :key="film.title">
      {{ idx+1 }} - {{ film.title }}
    </li>
  </ul>
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    title: 'Ghibli\'s Film',
    films: [
      {
        title: 'Castle in the Sky'
      },{
        title: 'Grave of the Fireflies'
      },{
        title: 'My Neighbor Totoro'
      }
    ]
  }
});
```

Kode contoh untuk mempelajari looping directive ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/GGqGvG](https://codepen.io/mazipan/pen/GGqGvG)

### 2.3.1.3 Show/Hide

Kita bisa juga melakukan show/hide suatu element HTML secara langsung dengan hanya menambahkan directive `v-show`. Menggunakan `v-show` ini sekilas memiliki efek yang sama dengan kita menggunakan `v-if`, namun yang berbeda adalah jika ketika kita menggunakan `v-if`, maka kita akan menghilangkan element tersebut dari DOM, sedangkan menggunakan `v-show` kita hanya akan menyembunyikan element tersebut tanpa benar-benar menghilangkan dari DOM.
Ini bermanfaat jika kita sering melakukan show/hide element, dibandingkan menggunakan `v-if` yang tentunya akan makan sumber daya yang lebih banyak.

Berikut contoh kode dari penggunakan directive show/hide:

```html
<div id="app">
  <button @click="isShow = !isShow">
    Show Message
  </button>
  <div v-show="isShow">{{ message }}</div>
  <div v-show="!isShow">{{ message2 }}</div>
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    message2: 'Now you see me!',
    isShow: false
  }
});
```

Kode contoh untuk mempelajari show/hide directive ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/JZRNbK](https://codepen.io/mazipan/pen/JZRNbK)

### 2.3.1.4 Rendering HTML

Ada kalanya kita harus merender HTML string kedalam komponen kita, karena bisa jadi response dari Rest API yang kita panggil mengembalikan nilai string dalam format HTML.
Untuk me-render HTML string kedalam HTML template, kita tidak bisa menggunakan sintaks interpolasi bisa dengan `{{ data }}`, karena dengan menggunakan ini maka data kita akan dianggap string biasa dan tidak dirender sebagai HTML.

Solusinya adalah memanfaatkan directive `v-html` untuk melakukan hal ini.

Berikut contoh kode rendering HTML dengan directive v-html:

```html
<div id="app">
  {{ message }}
  <br/>
  <span v-html="message"></span>
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    message: `<span style="color:red;">
                Hello Vue!
              </span>`
  }
});
```

Kode contoh untuk mempelajari `v-html` directive ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/WyGjzm](https://codepen.io/mazipan/pen/WyGjzm)

## 2.3.2 Custom Directives

Selain sudah tersedia berbagai Built-In directives, di Vue.js juga kita diberikan kemampuan untuk membuat directive kita sendiri sesuai dengan kebutuhan kita.

Namun memang untuk membuat custom directive tidak semudah menggunakan built-in directive karena kita mesti tahu terlebih dahulu bagaimana membuat custom directive, kemudian juga harus mempelajari bagaimana behaviour yang kita inginkan agar bisa membuat kode yang sesuai dengan apa yang diharapkan.

Berikut contoh kode membuat custom directive:

```html
<div id="app">
  <img :src="imgSrc">
  <img v-zoom :src="imgSrc">
</div>
```

```javascript
Vue.directive('zoom', {
  bind: function (el) {
    el.src = 'http://placekitten.com/300/300'
  }
});

const app = new Vue({
  el: '#app',
  data: {
    imgSrc: 'http://placekitten.com/100/100'
  }
});
```

Bisa dilihat di kode tersebut, kita memanfaatkan hook `bind` pada custom directive yang kita buat. Ini berarti kita ingin melakukan sesuatu ketika directive ini dibuat dan ditambahkan ke element bersangkutan. Hook `bind` ini hanya akan dieksekusi sekali saja.

Selain menggunakan hook `bind`, custom directive juga menyediakan hook lain yang bisa kita gunakan, seperti: `inserted`, `update`, `componentUpdate`, dan `unbind`. Penjelasan masing-masing hook ini bisa kalian temui di tautan official dokumentasi Vue.js di [https://vuejs.org/](https://vuejs.org/v2/guide/custom-directive.html#Hook-Functions)

Kode contoh untuk mempelajari custom directive ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/eKdWKp](https://codepen.io/mazipan/pen/eKdWKp)

## 2.4 Filters

`Filters` merupakan fitur yang diadopsi dari AngularJS. `Filters` merupakan fitur yang bisa melakukan manipulasi data seperti formatting berbagai data ke format data lain tanpa mengubah nilai dari data asalnya.
Pada prakteknya `filters` sangat dibutuhkan, apalagi untuk beberapa perusahaan seperti e-commerce yang pasti akan banyak kebutuhan untuk menampilkan harga dengan format yang ditentukan, atau tanggal dengan format yang telah di standarisasi oleh masing-masing. Menggunakan `filters` akan menghemat banyak waktu kita untuk melakukan hal yang sama berulang-ulang kali.

Berikut contoh kode untuk menggunakan filters:

```html
<div id="app">
  {{ price }}
  <br/>
  {{ price | currency }}
</div>
```

```javascript
const app = new Vue({
  el: '#app',
  data: {
    price: 5000
  },
  filters: {
    currency: function (value) {
      let result = "Rp " + (value).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
      return result
    }
  }
});
```

Kode contoh untuk mempelajari custom directive ini bisa dilihat di tautan berikut:

[https://codepen.io/mazipan/pen/aKmwow](https://codepen.io/mazipan/pen/aKmwow)

## 2.5 Plugins

`Plugins` seringkali digunakan untuk menambahkan kemampuan Vue.js untuk melakukan sesuatu di level global sehingga hal yang sering dilakukan tidak perlu didefinisikan berulang kali.

Bila kalian sudah pernah melihat kode projek Vue.js secara lengkap dan melihat ada kode dengan sintaks `Vue.use(plugin)`, itu adalah salah satu teknik untuk memasang suatu plugin ke lingkungan Vue yang sedang kita garap.

Pengetahuan membuat plugin ini menurut saya pribadi sangat penting terutama bila kita berurusan dengan codebase yang lumayan besar dan kompleks. Dengan menggunakan plugins ini, kita bisa memusatkan kode-kode yang berulang akan kita gunakan kedalam satu pembungkus plugins ini.

Kelebihan lain dari Plugins ini adalah bisa cross repository karena sifatnya yang biasanya independen dan terisolir. Ini memudahkan bagi kita yang mengurus banyak repository dengan kode yang hampir sama. Sehingga diharapkan di masa depan bila dibutuhkan perubahan, maka perubahan tersebut bisa dengan mudah dilakukan terhadap semua repository yang memang telah menggunakan plugin tersebut tanpa harus mengubah di banyak tempat atau repository.

Berikut contoh sederhana membuat plugin:

```html
<div id="app">
  {{ message }}
  <br>
  <button
    @click="clickMe">
    Click Me!
  </button>
</div>
```

```javascript
const MyPlugin = {
  install: function (Vue, options) {
    Vue.prototype.$myMethod = function (methodOptions) {
      alert('You fired me!')
    }
  }
}

Vue.use(MyPlugin)

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  },
  methods: {
    clickMe: function () {
      this.$myMethod()
    }
  }
});
```

Kode diatas bisa dilihat lebih detail di tautan berikut:

[https://codepen.io/mazipan/pen/ERgXZw](https://codepen.io/mazipan/pen/ERgXZw)

## 2.6 Mixins

`Mixins` merupakan fitur yang sering di abuse penggunaanya oleh banyak developer yang saya temui.
Hal ini tentu karena kemampuan `mixins` yang memperbolehkan kita meng-ekstrak bagian dari komponen yang kita buat agar bisa digunakan oleh komponen lain yang membutuhkan hal yang sama.

Berikut adalah contoh kasus bagaimana kita membuat `mixins` di Vue.js, katakanlah pada awalnya kita mempunyai dua komponen yang di dalamnya terdapat `data` dan `methods` yang sama, seperti terlihat pada potongan kode berikut:

```html
<div id="app">
  {{ message }}
  <br>
  <component1></component1>
  <component2></component2>
</div>
```

```javascript
Vue.component('component1', {
  data: function () {
    return {
      count: 0
    }
  },
  methods: {
    clickMe: function () {
      this.count++
    }
  },
  template: `<div>
              <button @click="clickMe">Click Me!</button>
              count {{ count }} in component1
            </div>`
});

Vue.component('component2', {
  data: function () {
    return {
      count: 0
    }
  },
  methods: {
    clickMe: function () {
      this.count++
    }
  },
  template: `<div>
              <button @click="clickMe">Click Me Too!</button>
              count {{ count }} in component2
            </div>`
});

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});
```

Kode diatas bisa dilihat lebih detail di tautan berikut:

[https://codepen.io/mazipan/pen/GGNEgB](https://codepen.io/mazipan/pen/GGNEgB)

Bisa kita lihat dari kode tersebut diatas, bahwa `component1` dan `component2` mempunyai data `count` dan methods `clickMe` yang isinya sama, dengan kasus ini kita akan coba pindahkan kode yang sama tersebut kedalam `mixin` sehingga bisa lebih mudah jika ada perubahan kedepannya. Berikut contoh kode setelah menggunakan `mixins`:

```html
<div id="app">
  {{ message }}
  <br>
  <component1></component1>
  <component2></component2>
</div>
```

Tidak ada perubahan sama sekali pada template yang kita buat, tapi mari kita bandingkan perubahannya pada JavaScript kita berikut ini:

```javascript
const myMixin = {
  data: function () {
    return {
      count: 0
    }
  },
  methods: {
    clickMe: function () {
      this.count++
    }
  }
}

Vue.component('component1', {
  mixins: [myMixin],
  template: `<div>
              <button @click="clickMe">Click Me!</button>
              count {{ count }} in component1
            </div>`
});

Vue.component('component2', {
  mixins: [myMixin],
  template: `<div>
              <button @click="clickMe">Click Me Too!</button>
              count {{ count }} in component2
            </div>`
});

const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});
```

Kedua komponen hanya cukup menambahkan `mixins: [myMixin]` dan secara otomatis keduanya akan memiliki hal yang sama seperti apa yang telah kita definisikan dalam `myMixin` sebelumnya.

Kode diatas bisa dilihat lebih detail di tautan berikut:

[https://codepen.io/mazipan/pen/qKqXaL](https://codepen.io/mazipan/pen/qKqXaL)


-------

> Objectif: Setelah membaca bagian ini diharapkan pembaca sudah bisa melakukan konfigurasi untuk menyiapkan dan praktek langsung serta mencoba berbagai kode dasar dari Vue yang akan lebih banyak lagi kita tampilkan pada bagian berikutnya.