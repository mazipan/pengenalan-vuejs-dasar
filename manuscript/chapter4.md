# Bagian 4 - Mudahnya Mengembangkan Web Vue.js

Pada bagian ini, para pembaca diharapkan sudah paham sepenuhnya soal dasar-dasar Vue.js dan berbagai pustaka pendukung di lingkungannya. Pada bagian ini akan dijelaskan berbagai contoh kasus, contoh masalah, berbagai optimalisasi, dan berbagai *best practice* yang mungkin bagi sebagian besar menjadi *nice to have* fitur namun sebenarnya akan sangat membantu kita bila kita mau memahami dan belajar hal-hal tersebut.

Mari kita mulai lagi perjalanan kita mempelajari Vue.js dengan pandangan yang lebih luas.

## 4.1 VSCode Extensions untuk Vue.js

Berikut adalah beberapa extensions untuk text editor VSCode yang bisa membantu kita dalam mengembangkan projek berbasiskan Vue.js, diantaranya:

- **Auto Close Tag** (oleh Jun Han)
- **Auto Rename Tag** (oleh Jun Han)
- **ESLint (oleh Dirk** Baeumer)
- **Import Cost** (oleh Wix)
- **Npm Intellisense** (oleh Christian Kohler)
- **Prettier - Code formatter** (oleh Esben Petersen)
- **Version Lens** (oleh pflannery)
- **Vetur** (oleh Pine Wu)
- **Vue Peek** (oleh Dario Fuzinato)

## 4.2 Tips dan Trik Vue.js

Berikut adalaha beberapa tips dan trik dari pengalaman saya selama ini bekerja dengan framework Vue.js:

- **Sepakati prioritas pada SFC**

Vue.js membuat Single File Component (SFC) agar mudah untuk kolaborasi dalam satu file antara UI developer dengan para JS developer. Meskipun secara fitur kita bebas untuk menuliskan tag `<template></template>`, `<script></script>` dan `<style></style>` dengan urutan apapun, tapi dari pengalaman yang saya jalani selama ini bahwa hal ini akan lebih baik bila bisa disepakati dengan semua anggota tim. Bila telah disepakati urutan yang akan digunakan, maka sebagai developer yang baik, ikutilah aturan hasil kesepakatan tersebut secara konsisten. Tujuan hal ini harus disepakati adalah agar kode kita semakin konsisten dan mudah untuk ditebak posisi dari suatu kode. 

Selain posisi tag utama pada SFC, kita juga perlu sepakat mengenai urutan yang seharusnya diterapkan dalam membuat komponen. Misalnya saya sering menggunakan urutan berikut:

```bash
> components 
> mixins 
> props
> data 
> computed 
> watch 
> beforecreated 
> created 
> beforemount 
> mounted 
> destroyed
```

- **Menjadikan `computed` sebagai pilihan pertama**

`computed` tidak bisa menyelesaikan semua kebutuhan kita, tapi dengan ringannya membuat kode `computed` maka saya sarankan untuk mengutamakan menggunakan `computed` dibanding hal lain baik `methods` ataupun `watch`.

## 4.4 Membuat Loose Couple Components

TODO

## 4.5 Membuat Plugin Vue.js Sendiri

TODO

## 4.6 Melakukan Optimasi pada Projek Vue

## 4.6 Unit Testing Vue.js

TODO

-------

> Objectif: Setelah membaca bagian ini diharapkan pembaca sudah bisa melakukan konfigurasi untuk menyiapkan dan praktek langsung serta mencoba berbagai kode dasar dari Vue yang akan lebih banyak lagi kita tampilkan pada bagian berikutnya.