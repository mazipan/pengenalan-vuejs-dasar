# Bagian 5 - Latihan Membuat Projek Vue.js

## 5.1 Membuat Projek Vue.js

TODO FULL OF SOURCE CODE


## 5.2 Referensi Vue.js Bahasa Indonesia

[Medium](https://medium.com/vuejs-id) - Berisi tutorial dan insight dalam bentuk tulisan berbahasa Indonesia.
[Video Tutorial](https://vuejs.id/video-tutorial/) - Belajar Vue.js melalui video tutorial berbahasa Indonesia.


## 5.3 Komunitas Vue.js Indonesia

- [Facebook](https://www.facebook.com/groups/1675298779418239/) - Komunitas Vue.js Indonesia di Facebook.
- [Telegram](https://t.me/vuejsid) - Komunitas Vue.js Indonesia di Telegram messenger.
- [Github](https://github.com/vuejs-id) - Komunitas Vue.js Indonesia di Github.

