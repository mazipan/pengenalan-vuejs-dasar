# Bagian 6 - Penutup

## 6.1 Tentang Penulis

![Irfan Maulana](https://pbs.twimg.com/profile_images/766850033090080768/Tvi6Y_t1_200x200.jpg)

Saya Irfan Maulana, seorang pengembang web asal Pemalang, Indonesia.

Memiliki pengalaman bekerja lebih dari 5 tahun dan saat ini bekerja sebagai Senior Software Development Engineer di Bizzy Indonesia. Sebelumnya pernah bekerja di Blibli.com selama hampir 3 tahun dan SML Technologies selama 2 tahun lebih.

Mengkhususkan diri pada bidang pengembangan antar muka halaman website dengan pengetahuan seputar HTML, CSS JavaScript dan berbagai teknologi lain pendukungnya.

Selain sebagai *full time* pengembang web, saya juga aktif sebagai blogger untuk menulis berbagai artikel dan tutorial baik teknikal maupun non-teknikal yang sebagian besar saya tulis dalam Bahasa Indonesia.

Bila ada saran, kritik atau pertanyaan mengenai ebook ini, silahkan menghubungi penulis di:

Email		: [mazipanneh@gmail.com](mailto:mazipanneh@gmail.com)
Blog		: [mazipanneh.com](http://mazipanneh.com/blog)
Github	: [mazipan](https://github.com/mazipan)
Facebook: [/mazipanneh](http://www.facebook.com/mazipanneh)
Twitter	: [@Maz_Ipan](http://www.twitter.com/maz_ipan)
Linkedin: [/in/irfanmaulanamazipan](http://www.linkedin.com/in/irfanmaulanamazipan)


