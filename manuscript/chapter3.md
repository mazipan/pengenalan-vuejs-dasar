# Bagian 3 - Menyelami Lingkungan Vue.js

Pada bagian kali ini kita akan belajar berbagai perangkat yang ada disekitar pustaka utama Vue.js. Hal ini diperlukan karena ketika terjun kedalam projek yang kompleks, maka hal-hal berikut mungkin akan jadi suatu keharusan untuk digunakan. Hal yang akan kita bahas meliputi *Vue-Router*, *Vuex*, *Event Bus*, *i18n*, komunikasi dengan *Rest API*, samapai mempelajari cara *bundling* dan *deployment* projek Vue.js.

Untuk masuk ke materi bagian ini diharapkan pembaca sudah menguasai bagian sebelumnya yang merupakan bekal untuk mempelajari bagian kali ini. Karena pada bagian ini, tidak akan lagi dijelaskan mengenai berbagai hal dasar yang sudah dijelaskan pada bagian sebelumnya. Pada bagian ini pula akan ditampilkan banyak contoh yang sudah menggunakan lingkungan JavaScript yang modular, sehingga berbagai perintah baik dari `npm` ataupun `Yarn` akan banyak kita temui. Para pembaca diharapkan sudah mulai mempelajari dasar-dasar dari `npm` atau `Yarn` itu sendiri agar tidak ketinggalan dengan materi yang disajikan pada bagian ini.

Mari kita teruskan perjalanan kita untuk mengenali dan belajar berbagai hal yang berada di lingkungan pengembangan web dengan Vue.js.


## 3.1 Vue Router

[Vue Router](https://router.vuejs.org/) merupakan official *router* untuk Vue.js. Fungsinya sama dengan *router* pada *framework* JavaScript lainnya yakni untuk mengatur navigasi halaman di bagian antar muka sehingga tidak lagi dibutuhkan *redirect* URL di bagian *backend* lagi. Seperti pada *router* yang lain, `Vue Router` ini juga mendukung mode *history* dan mode menggunakan *hash* sehingga memberikan pilihan pada pengguna untuk memilih mode mana yang cocok bagi aplikasi yang sedab dibangun. Mode *history* akan membuat web yang kita buat nampak seperti web normal yang memiliki backend sebagai *router*, sedangkan mode *hash* akan menambahkan tagar pada bagian belakang URL.


## 3.1.1 Memasang Vue Router

Menurut saya pribadi, `Vue Router` akan sangat membantu bila kita memasang pada projek yang menggunakan *build tool* seperti *webpack* karena kita bisa memaksimalkan berbagai keunggulan seperti menggunakan berbagai sintaks *ES6+* dengan mudah, memecah file komponen sesuai dengan lokasinya. Namun pada contoh kali ini kita akan campurkan cara memasang `Vue Router` langsung di browser menggunakan eksternal script dan juga mengunakan lingkungan *webpack*.


### 3.1.1.1 Memasang Vue Router di Browser

Untuk memasang di browser, pertama selain menambahkan eksternal script Vue, kita juga mesti menambahkan eksternal script `Vue Router` yang bisa didapatkan di berbagai CDN, seperti berikut:

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
```

Setelahnya kita bisa membuat template untuk aplikasi yang akan kita jelaskan fungsinya baris demi baris, berikut contoh template aplikasi kita:

```html
<div id="app">
  <h1>{{ message }}</h1>

  <p>
    <router-link to="/foo">Go to Foo</router-link>
    <router-link to="/bar">Go to Bar</router-link>
  </p>

  <router-view></router-view>
</div>
```

Dari kode diatas kita menemukan beberapa tag baru yakni `<router-link></router-link>`, tag ini nantinya akan di-*render* di HTML sebagai tag `a` atau akan menghasilkan tautan seperti biasa dengan attribute `href` yang otomatis dicocokan oleh `Vue Router` berdasarkan `props` yang diterimanya, seperti pada contoh diatas kita menggunakan `to` untuk menentukan tujuan dari *link* tersebut.

Kita juga menemukan tag `<router-view></router-view>`, dimana tag ini akan menjadi penampung untuk menampilkan komponen yang memenuhi syarat persamaan. Setidaknya dua tag baru ini adalah dasar yang harus kita ketahui dari pembuatan `Vue Router` secara sederhana.

Setelah membuat template di HTML, kita akan mendefinisikan *router* kita di JavaScript, seperti contoh kode berikut:

```js
const Foo = { template: '<div>I am foo</div>' }
const Bar = { template: '<div>I am bar</div>' }

const router = new VueRouter({
  routes: [
    { path: '/foo', component: Foo },
    { path: '/bar', component: Bar }
  ],
  mode: 'hash'
})

const app = new Vue({
  router,
  el: '#app',
  data: {
    message: 'Hello Vue Router!'
  }
});
```

Dari kode diatas kita bisa tau bahwa untuk medefinisikan sebuah *router* menggunakan `Vue Router` kita diharuskan membuat instance `new VueRouter()` dengan memberikan objek yang menjadi konfigurasi dari *router* kita. Konfigurasi paling minimal adalah kita diharuskan mendeskripsikan semua route yang akan kita gunakan, pada contoh diatas kita mendinisikan dua URL route `/foo` dan `/bar` yang keduanya akan menggunakan dua komponen yang berbeda untuk ditampilkan. Kita juga bisa menambahkan mode yang akan kita pilih untuk router kita, pada contoh kode diatas saya memilih menggunakan *hash* dimana ini sebenarnya merupakan standar yang dibuat oleh `Vue Router` sehingga kita tidak diharuskan untuk menuliskannya bila menggunakan mode ini. Setelah membuat instance dari `Vue Router`, jangan lupa untuk menyematkan kedalam instance Vue yang kita buat untuk aplikasi kita.


### 3.1.1.2 Memasang Vue Router di Webpack

Untuk memasang `Vue Router` di webpack yang paling pertama tentu kita harus menambahkan *dependency* ke dalam projek kita. Hal ini bisa kita lakukan dengan menggunakan perintah:

```bash
npm install vue-router
# atau dengan Yarn
yarn add vue-router
```

Setelah mengetikkan perintah ini maka kita akan mengunduh *dependency* `vue-router` kedalam folder `node_modules` dan menambahkan satu baris *dependency* baru di package.json kita.

Setelahnya kita bisa membuat folder baru dibawah `src` dengan nama `router` dan menambahkan file `index.js` yang berisi kode konfigurasi `router` kita seperti berikut:

```js
const Foo = { template: '<div>I am foo</div>' }
const Bar = { template: '<div>I am bar</div>' }

const routerConfig = new VueRouter({
  routes: [
    { path: '/foo', component: Foo },
    { path: '/bar', component: Bar }
  ],
  mode: 'hash'
})

export default routerConfig
```

File ini kemudian bisa kita panggil di `main.js` dengan ditambahkan inisialisasi penggunaan `vue-router`, seperti terlihat pada contoh kode berikut:

```javascript
import Vue from 'vue'
import Router from 'vue-router'

import routerConfig from './router'
import App from './App.vue'

Vue.use(Router)

new Vue({
  el: '#app',
  router: routerConfig,
  template: '<App/>',
  components: {App}
})
```

Terakhir kita tinggal menambahkan template di `App.vue` dengan kode yang berisi `<router-link></router-link>` dan `<router-view></router-view>` seperti terlihat pada potongan kode berikut:

```html
<div id="app">
  <h1>{{ message }}</h1>

  <p>
    <router-link to="/foo">Go to Foo</router-link>
    <router-link to="/bar">Go to Bar</router-link>
  </p>

  <router-view></router-view>
</div>
```

## 3.1.2 Vue Router Tingkat Lanjut

`Vue Router` seperti dijelaskan diatas merupakan pustaka pendukung bagi projek Vue.js yang kehadirannya sering sekali kita temui. Berikut akan dijabarkan beberapa topik dengan tingkat yang lebih lanjut mengenai penggunaan `Vue Router` di dalam projek.


### 3.1.2.1 Parameter pada Router

Seringkali kita harus membuat satu *route* dengan URL yang bisa dilemparkan suatu parameter seperti id atau berbagai data unik lainnya. Adapun bila kita ingin menambahkan *query param* yang biasanya ditandai dengan karakter`?` atau `&`, kita tidak diharuskan untuk mendefinisikan parameter tersebut didalam inisialisasi. Berbeda bila yang kita ingin buat adalah parameter *path* seperti contoh `/user/{id}`, kita harus mendefinisikan terlebih dahulu di dalam file konfigurasi *router* kita seperti terlihat pada contoh kode berikut ini:

```javascript
const User = {
  template: '<div>User</div>'
}

const router = new VueRouter({
  routes: [
    {
      path: '/user/:id',
      component: User
    }
  ]
})
```

Bisa kita lihat dari kode diatas, kita perlu menambahakan karakter titik dua atau *semicolon* (:) sebelum mendefinisikan parameter *path* yang dibutuhkan oleh *route* tersebut.
Untuk mendapatkan nilai `id` di komponen kita bisa mengambil lewat instance dari route tersebut seperti: 

```javascript
this.$route.params.id
```

Sedangkan untuk mendapatkan nilai dari query param misalkan kita punya URL berikut `/user?active=true`, kita bisa menggunakan kode berikut:

```javascript
this.$route.query.active
```

### 3.1.2.2 Memberi Nama pada Router

Menghafal sebuah pola URL seringkali menjadi hal yang susah terlebih lagi bila URL yang kita definisikan semakin banyak yang berakibat semakin banyak pula pola URL yang harus kita ingat. Menanggapi masalah ini `Vue Router` menyediakan fitur untuk memberikan nama pada *route* yang kita buat dengan tujuan memudahkan kita saat melakukan navigasi antar halaman dan juga menghindarkan kita dari keharusan mengingat pola URL yang seringkali lebih panjang dan sukar diingat. Berikut adalah contoh kode untuk memberikan nama atau alias pada *route*:

```javascript
const User = {
  template: '<div>User</div>'
}

const router = new VueRouter({
  routes: [
    {
      path: '/user/:userId',
      name: 'user',
      component: User
    }
  ]
})
```

Dengan memberikan nama maka kita bisa melakukan navigasi dengan lebih mudah tanpa perlu mengingat pola URL dari *route* tersebut, seperti pada contoh kode berikut:

```html
<router-link
  :to="{ name: 'user', params: { userId: 123 }}">
    User
</router-link>
```

### 3.1.2.3 Redirect Router

Kita bisa melakukan *redirect* atau pengalihan dari satu halaman ke halaman lain lewat konfigurasi di `Vue Router`. Pengalihan halaman yang dimaksud adalah bahwa ketika kita mengunjungi salah satu URL katakanlah `/a` maka *router* akan secara otomatis mengalihkan URL yang kita kunjungi menjadi `/b`. Berikut contoh kode paling sederhana dari membuat pengalihan halaman dengan `Vue Router`:

```javascript
const router = new VueRouter({
  routes: [
    {
      path: '/a',
      redirect: '/b'
    }
  ]
})
```

### 3.1.2.3 Navigasi Router Manual

Bila pada contoh sebelumnya kita juga sudah mempelajari perpindahan halaman menggunakan `vue-router` bahwa bisa bekerja secara otomatis dengan menggunakan `<router-link></router-link>` dan mengecek URL pada props `to` yang didefinisikan sebelumnya. Pada seksi ini kita akan belajar cara lain yang sering digunakan oleh para pengembang untuk memindahkan halaman di dalam `vue-router`.

Kita bisa langsung melakukan navigasi manual lewat instance dari `Vue Router` yang kita buat, atau bila kita ingin mengakses lewat komponen atau instance dari Vue itu sendiri maka kita bisa mengakses *router* ini lewat `this.$router`. Sedangkan untuk melakukan navigasi antar halaman, kita bisa memanfaatkan method `.push` atau `.go`.

`.push` biasa digunakan ketika kita ingin pindah dari satu halaman ke halaman lain tanpa memperdulikan apakah kita sudah pernah mengunjungi URL tersebut atau belum. Menggunakan `.push` berarti kita akan selalu melakukan `append` terhadap object `history` yang ada di *browser*. Sedangkan `.go` digunakan dengan memperhatikan kunjungan kita, bila kita melempar nilai positif maka kita akan mengunjungi halaman selanjutnya yang sudah kita kunjungi dan bila kita mlempar nilai negatif maka kita akan mengunjungi halaman sebelumnya yang kita telah kunjungi.

Berikut contoh melakukan navigasi manual dengan menggunakan `.push`:

```javascript
// literal string path
router.push('/user')

// object
router.push({ path: '/user' })

// named route
router.push({ name: 'user', params: { userId: 123 }})

// with query, resulting in /register?plan=private
router.push({ path: '/register', query: { plan: 'private' }})
```

Dan berikut contoh menggunakan `.go`:

```javascript
// go forward by one record, the same as history.forward()
router.go(1)

// go back by one record, the same as history.back()
router.go(-1)
```

## 3.2 Vuex - State Management

**Vuex** mungkin bukan sebuah library official dari Vue.js team, namun merupakan *de facto* state management yang paling banyak digunakan oleh pengembang web Vue.js. `Vuex` bisa mengelola dan meletakan state ditengah-tengah sehingga setiap komponen tidak perlu lagi pusing untuk saling melempar data. Cukup berikan perubahan yang diinginkan ke `vuex` dan semua yang membutuhkan data tersebut akan mendapatkan perubahan data yang diinginkan. `Vuex` bisa jadi berguna (sebagian menjadi terlalu berguna) ketika kita harus mengelola projek yang kompleks dengan aliran data yang kompleks pula. `Vuex` berhasil menengahi keterbatasan komponen Vue.js yang hanya bisa mengkonsumsi data dari lokal komponennya sendiri.

![Ilustrasi Vuex](https://vuex.vuejs.org/vuex.png)


## 3.2.1 Kapan harus menggunakan Vuex?

Meskipun `Vuex` bisa jadi sangat berguna dan bisa jadi menjadi pusat dari segala komunikasi data kita, namun perlu dipahami juga bahwa tidak semua hal atau aksi itu harus lewat `vuex`. Bahkan pada level yang ekstrim, kita bisa mengatakan bahwa tidak semua projek Vue.js membutuhkan kehadiran `vuex`. Kembali sebagai fungsinya, `vuex` sebagai state management telah mengadopsi arsitektur yang baik agar segala perubahan pada state bisa lebih mudah diketahui dan diprediksi. Masalahnya `vuex` juga membutuhkan *boilerplate* yang lumayan *heavy* untuk sebuah projek, sehingga diharapkan kita bisa lebih bijak dalam mengambil keputusan. Bila projek yang sedang kita buat tidak sebegitu kompleks dan tidak terlalu membutuhkan kehadiran `vuex`, mungkin kita bisa menggunakan opsi yang lebih sederhana seperti **Event Bus** (akan dijelaskan berikutnya).

Saat yang paling tepat untuk memutuskan menggunakan `vuex` adalah ketika aplikasi yang kita buat telah menjadi semakin kompleks dengan banyak data yang saling terkait antara satu dengan banyak komponen lain. Maka menggunakan *centralize state* menjadi pilihan yang terbaik.

Setelah memutuskan memasang `vuex`, pun kita masih dihadapkan pada keputusan state apa yang layak diletakkan di dalam `vuex` ini. Karena memang tidak seharusnya semua state harus ada di dalam `vuex`. State yang paling ideal ada di `vuex` adalah state yang banyak dikonsumsi oleh banyak komponen yang mana tidak saling terikat hubungan *parent-child* dalam strukturnya.


## 3.2.2 Memasang Vuex

`Vuex` dibuat dalam bentuk plugin untuk Vue.js agar mudah bagi kita untuk memasang dalam projek yang membutuhkannya. Seperti sebelumnya, langkah pertama kita harus memasang dependency lewat `npm` ataupun `yarn` dengan perintah:

```bash
npm i vuex --save
# atau
yarn add vuex
```

Setelahnya kita bisa memasang plugin `vuex` kedalam projek kita dengan kode:

```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})

new Vue({
  el: '#app',
  store,
  template: '<App/>',
  components: {App}
})
```

Pada contoh kode diatas kita langsung membuat store di file yang sama, pada kasus nyata hal ini sangat jarang dilakukan. Pengembang lebih senang memisahkan ke file lain yang nantinya bisa dengan mudah diimport untuk di-*inject* ke dalam instance Vue yang akan dibuat.


## 3.2.3 Istilah penting di Vuex

Ada beberapa istilah penting di Vuex yang pasti akan kalian sering kalian temui ketika memilih untuk menggunakan Vuex di project yang kalian bangun.

Beberapa istilah itu antara lain:


### 3.2.3.1 Vuex Store

*Vuex Store* atau agar lebih singkat kita sebut *store* saja adalah instance dari Vuex itu sendiri, dimana didalamnya terdapat komponen yang menghimpun sebuah *store* seperti `state`, `getters`, `mutations` dan `actions`.

Berikut contoh kode dari *store* sederhana:

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})
```


### 3.2.3.2 State

`State` merupakan variabel-variabel yang akan didistribusikan dari satu komponen ke komponen lain. `State` di dalam `vuex` hanya boleh diubah oleh internal `store` itu sendiri yakni melalui `mutations` namun kita masih bisa membaca `state` dari luar `store` dengan akses sebatas *read-only*.


Berikut contoh kode dari *state* sederhana:

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  }
})
```

Untuk membaca nilai `state` dari sebuah komponen kita bisa mengakses lewat instance store yang sudah di-inject kedalam Vue instance sehingga bisa lewat `this.$store`. Berikut kode untuk mengakses state di dalam komponen:

```javascript
export default {
  computed: {
    count: function () {
      return this.$store.state.count
    }
  }
}
```

Untuk memastikan bahwa setiap perubahan yang terjadi si dalam `state` bisa terima di dalam komponen, kita harus meletakan di dalam `computed`.


### 3.2.3.3 Getters

`Getters` memiliki fungsi untuk mengambil nilai dari `state` yang berada di `store`. Hal ini dikarenakan sifat `state` yang tidak bisa diakses dari luar `store` sehingga kita hanya bisa mengaksesnya lewat `getters` yang kita buat. `Getters` biasanya akan mengembalikan nilai berupa isi dari `state` yang ingin kita ambil, namun begitu kitapun bisa menyelipkan sedikit logika di dalam `getters` ini bila memang diperlukan.


Berikut contoh kode dari *state* sederhana:

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  getters: {
    getCount: state => {
      return state.count
    },
    getCountPlusOne: state => {
      return state.count + 1
    }
  }
})
```

Pada kode diatas kita membuat dua `getters` untuk mendapatkan nilai `state` `count` yang telah kita definisikan sebelumnya yakni `getCount` dan `getCountPlusOne`. Pada `getters` `getCount` kita hanya mengembalikan nilai `state` apa adanya, sedangkan pada `getCountPlusOne` kita melakukan sedikit modifikasi pada nilai yang dikembalikan tanpa melakukan perubahan pada nilai `count` aslinya.

Untuk mengakses `getters` dari komponen kita juga harus meletakkan di dalam `computed` seperti kita mengakses `state` karena setiap perubahan yang terjadi harus ditangkap oleh komponen yang membutuhkan.

Berikut contoh sintaks mengakses `getters` dari komponen:

```javascript
export default {
  computed: {
    getCount: function () {
      return this.$store.getters.getCount
    },
    getCountPlusOne: function () {
      return this.$store.getters.getCountPlusOne
    }
  }
}
```


### 3.2.3.4 Mutations

`Mutations` merupakan bagian dari `store` yang berfungsi untuk melakukan perubahan nilai pada sebuah `state`, jadi semua perubahan nilai terhadap `state` hanya boleh dilakukan lewat `mutation` ini. Untuk memanggil sebuah `mutation` hanya bisa dilakukan dengan cara mengirimkan `commit` diikuti nama `mutation` dan `payload` yang kita ingin kirimkan kepada `store` tersebut.


Berikut contoh kode dari *store* sederhana:

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    },
    setCount (state, value) {
      state.count = value
    }
  }
})
```

Pada kode diatas kita membuat dua `mutations` yang keduanya akan mengubah nilai state kita, yang pertama kita membuat `increment` yang berfungsi menaikan nilai dari `count` yang kita punya dan `setCount` yang berfungsi menyetel nilai pada state `count` kita.

Untuk mengakses `mutations` dari komponen kita cukup meletakan di `methods` dan dipanggil sebagai aksi biasa di dalam komponen.


Berikut contoh kode mengakses `mutations` dari komponen:

```javascript
export default {
  methods: {
    increment: function () {
      this.$store.commit('increment')
    },
    setCount: function (param) {
      this.$store.commit('setCount', param)
    }
  }
}
```


### 3.2.3.5 Actions

`Actions` merupakan bagian di dalam `store` yang biasanya digunakan untuk melakukan suatu aksi tertentu, baik mengubah nilai state ataupun melakukan berbagai async proses yang memang tidak didukung oleh `mutations`. Kita bisa memanggil sebuah `actions` dengan cara mengirimkan sebuah `dispatch` diikuti nama `actions` tersebut.


Berikut contoh kode dari *store* sederhana:

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    },
    setCount (state, value) {
      state.count = value
    }
  },
  actions: {
    doIncrement ({ commit }) {
      commit('increment')
    },
    doSetCount ({ commit }, value) {
      commit('setCount', value)
    }
  }
})
```


## 3.2.4 Tips dan Trik Mengenai Vuex

Berikut beberapa trik dan tips dalam menggunakan `vuex` berdasarkan pengalaman saya selama ini:


- **Gunakan Spread Operator**

Kita bisa memanfaatkan [spread operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax) yang sudah disediakan oleh `vuex` yang akan memudahkan kita dalam mengakses `state`, `getters`, `mutations` maupun `actions` jika jumlah yang harus diakses sudah semakin banyak. 


Berikut contoh kode menggunakan *spread operator* dari contoh kode yang sudah kita buat diatas:

```javascript
import { mapState, mapGetters, mapMutations, mapActions } from 'vuex'

export default {
  computed: {
    ...mapState([
      'count'
    ]),
    ...mapGetters([
      'getCount',
      'getCountPlusOne'
    ])
  },
  methods: {
    ...mapMutations([
      'increment',
      'setCount'
    ]),
    ...mapActions([
      'doIncrement',
      'setCount'
    ])
  }
}
```


- **Jangan Memanggil Mutations dari Komponen**

Meskipun secara fitur, kita dibolehkan untuk memanggil `mutations` diluar `store` seperti di dalam komponen, namun dengan adanya `actions` saya sendiri lebih merekomendasikan untuk hanya memanggil `actions` dari luar `store` bilamana ingin melakukan perubahan pada nilai `state`. Untuk kemudian secara internal, `actions` tersebut memanggil `mutations` yang diinginkan. Hal ini untuk membuat *single source* dari perubahan yang terjadi pada `state` kita, sehingga lebih mudah bagi kita untuk memprediksi dari mana asal datangnya perubahan suatu `state`.

Jadi kalau pada contoh sebelumnya kita mempunyai `mutations` `increment`, saya tidak menyarankan kita melakukan `commit` secara langsung dari sebuah komponen. Lebih baik bila kita membuat `actions` sebagai wrapper dari `mutations` ini, sehingga kita membuat lagi `actions` `doIncrement` yang sebenarnya hanya melakukan `commit` terhadap `mutations` yang bersangkutan.


- **Sederhanakan Kode di dalam Store**

Bagian yang biasanya menjadi titik terberat dari sebuah `store` di `vuex` terletak pada `actions`, padahal di dunia nyata seringkali satu file `actions` akan terdiri dari banyak fungsi yang nantinya akan kita akses dari komponen. Menjadi susah bagi kita untuk mengetahui di dalam file tersebut ada `actions` apa saja yang tersedia bila kode di masing-masing `actions` terdiri dari banyak baris. Jadi trik dan sarannya adalah memindahkan isi logika dari `actions` tersebut ke file lain dan menyisakan beberapa baris saja untuk memanggil file bersangkutan.


## 3.3 Event Bus

`Event Bus` merupakan sebuah fitur dari Vue yang memanfaatkan *instance* dari Vue itu sendiri untuk bisa melakukan *sharing* data maupun aksi dari satu komponen dengan komponen lain. `Event bus` mungkin tidak se*powerfull* `vuex` dalam melakukan management state didalam aplikasi kita, namun kemampuan untuk berbagi data dan aksi saya rasa sudah cukup membantu bagi aplikasi yang tidak sebegitu kompleknya sehingga tidak dibutuhkan `vuex` disana. `Event bus` secara sederhana bisa digambarkan lewat contoh kode-kode berikut:

Pertama, kita akan membuat file `event-bus.js` yang berisi instance Vue baru dan menambahkan listener dengan menggunakan `$on` seperti kode dibawah ini:

```javascript
import Vue from 'vue';
const EventBus = new Vue();

EventBus.$on('onCallEventBus', param => {
  console.log(`You call me ${param}`)
});

export default EventBus
```

Setelahnya, kita bisa menggunakan file ini dan melakukan `import` di komponen yang membutuhkan dan memanggil berbagai event yang tersedia dengan menggunakan `$emit` seperti contoh kode berikut:

```javascript
import EventBus from './event-bus.js';

export default {
  data() {
    return {}
  },
  methods: {
    onClickMe() {
      EventBus.$emit('onCallEventBus', 'father');
    }
  }
}
```

Dari kode diatas kita bisa melihat bahwa `EventBus` bisa dipanggil dimanapun kita inginkan dan bisa berlaku sebagai pembagi berbagai aksi yang berlaku global. Pada contoh diatas kita membuat contoh untuk memanggil suatu *event* di `methods` dari dalam sebuah komponen.


## 3.4 Internationalization (i18n)

`i18n` merupakan fitur dari sebuah aplikasi untuk mendukung berbagai jenis bahasa untuk lebih menggapai banyak pengguna dari berbagai macam negara. Hal ini coba kita bahas pada buku ini, karena memang dewasa ini kebutuhan akan `i18n` ini menjadi hal yang umum diharapkan dalam suatu aplikasi/web.

Di Vue sendiri, hal ini sudah lebih mudah diimplementasikan, terutama dengan adanya berbagai library dan plugins yang dibuat oleh orang lain yang pastinya membantu kita dalam mendukung `i18n` ini. Pada buku ini kita akan mempelajari cara-cara mendukung i18n di Vue menggunakan salah satu library yang cukup populer yakni `Vue-i18n`, berikut cara-cara menggunakan `vue-i18n`pada sebuah projek Vue.js:


### 3.4.1 Memasang dan Menyetel Vue-i18n

`Vue-i18n` dibuat dalam bentuk plugin juga yang kurang lebih akan sama dengan `Vuex` dan `Vue-Router` cara memasangnya. Pertama, kita akan menambahkan dependency kedalam projek kita dengan perintah:

```bash
npm i vue-i18n --save
# atau
yarn add vue-i18n
```

Setelahnya, kita bisa melakukan inisiasi i18n dengan cara:

```javascript
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import App from './App.vue'

Vue.use(VueI18n)

const langEN = require('./locales/en');
const langID = require('./locales/id');

const i18n = new VueI18n({
  locale: 'id',
  fallbackLocale: 'id',
  messages: {
    en: langEN,
    id: langID,
  },
})

new Vue({
  el: '#app',
  store,
  template: '<App/>',
  components: {App}
})
```

Kita harus membuat file untuk menyimpan berbagai kata yang akan dibuatkan translasinya. Dari contoh diatas kita akan mendukung dua macam bahasa yakni bahasa Inggris dan bahasa Indonesia. 

Berikut contoh file untuk bahasa Indonesia, yang kita letakkan di folder `/locales/id.js`: 

```javascript
export default {
  welcome: 'Selamat Datang',
  hello: 'Halo, {name}'
}
```

Berikut contoh file untuk bahasa Inggrisnya, yang kita letakkan di folder `/locales/en.js`: : 

```javascript
export default {
  welcome: 'Welcome',
  hello: 'Hello, {name}'
}
```

Pada kedua kode diatas, kita membuat dua kunci yang nantinya akan kita panggil yakni `welcome` dan `hello`, dimana yang satunya dengan kunci `hello` adalah contoh ketika satu kunci yang membutuhkan parameter untuk diisi bila ingin digunakan oleh komponen nantinya.


### 3.4.2 Memanggil I18n dalam Komponen

Setelah berhasil memasang plugin `vue-i18n` tentu kita harus tau bagaimana cara menggunakan ketika dibutuhkan. Tempat yang seringkali menggunakan `i18n` ini adalah komponen yang biasanya bisa langsung menggunakan di dalam template ataupun lewat script.

Berikut contoh bagaimana kita menggunakan `i18n` tersebut:

```html
<template>
  <h1> {{ $t('welcome') }} </h1>
  <h3> {{ $t('hello', {name: 'Irfan Maulana'}) }} </h3>
</template>
```

Pada kode diatas juga diberikan contoh bagaimana memanggil suatu bahasa yang tersedia lewat kunci yang di definisikan sebelumnya. Juga terdapat contoh untuk melempar parameter kedalam translasi tersebut.

Selain digunakan secara langsung pada bagian template, kita juga bisa memanggil lewat script yang bisa ditempatkan dimanapun. Pada contoh kode berikut ini kita akan meletakan pada `methods`:


```javascript
export default {
  methods: {
    getWelcome: function () {
      return this.$t('welcome');
    },
    getHello: function () {
      return this.$t('hello', {name: 'Irfan Maulana'});
    }
  }
}
```


## 3.5 Komunikasi dengan Rest API

Rest API selama bertahun-tahun sudah menjadi standard bagi komunikasi antar backend-frontend maupun backend-backend, karenanya pada buku ini tidak akan lengkap rasanya kalau kita tidak membahas bagaimana cara melakukan komunikasi dengan Rest API pada projek Vue.js. Ada beberapa alternatif tentunya, tapi kita akan coba bahas beberapa yang cukup tenar saja, serta kemungkinan akan diimplementasikan di projek kalian. Berikut beberapa diantaranya:


## 3.5.1 Menggunakan Fetch

[Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) merupakan API bawaan browser modern yang bisa kita manfaaatkan untuk melakukan request terhadap suatu Rest API endpoint. Dengan memilih menggunakan fetch berarti kita sudah menghemat dengan tidak melakukan penambahan *third-party library* pada projek kita. Namun begitu memang dukungan akan API fetch ini tidak ada diseluruh browser meskipun sebagian besar browser modern sebenarnya sudah mendukung fitur ini. Jadi bila pengguna aplikasi kalian masih *stuck* dengan browser lama, maka tidak disarankan untuk menggunakan `fetch`.

Berikut adalah contoh melakukan request kepada salah satu Rest API endpoint menggunakan `fetch`:

```javascript
const API_URL = 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Fwwwid'

fetch(API_URL)
  .then(resp => resp.json())
  .then(data => {
    console.log(data.items)
  })
  .catch(error => console.error(error));
```

Sekali lagi perlu kalian ketahui bahwa `fetch` ini belum bisa digunakan disemua browser meskipun sebagian besar browser modern sudah mendukung API ini, kalian bisa melihat dukungan browser terhadap `fetch` di tautan [https://caniuse.com/#feat=fetch](https://caniuse.com/#feat=fetch).

Bila kalian tetap ingin menggunakan `fetch` namun masih ragu dengan dukungan browser, kalian bisa menambahkan *polyfill* kedalam projek kalian dari tautan berikut:

[https://github.com/github/fetch](https://github.com/github/fetch)

Dokumentasi lebih lengkap mengenai `fetch` bisa dilihat di tautan berikut
[https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch)


## 3.5.2 Menggunakan Axios

[Axios](https://github.com/axios/axios) merupakan *third-party library* yang sangat populer digunakan oleh para pengembang untuk berkomunikasi dengan Rest-API. Sebagai *third-party library*, axios merupakan salah satu yang memiliki fitur paling komplit. Karena popularitasnya ini pula, kita memilih untuk membahas penggunaan library ini dibanding library serupa.

Untuk menggunakan `axios` yang pertama harus kita lakukan adalah menambahkan *dependency* kedalam projek kita dengan perintah:

```bash
npm i axios --save
# atau
yarn add axios
```

Dan berikut adalan contoh sederhana menggunakan `axios` untuk melakukan request sebuah data:

```javascript
const axios = require('axios')
const API_URL = 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Fwwwid'

axios.get(API_URL)
  .then(function (data) {
    console.log(data.items)
  })
  .catch(function (error) {
    console.log(error)
  });
```

Contoh penggunaan axios di dalam project Vue.js bisa juga kalian lihat di YouTube channel dari Mas Mulia Nasution di tautan berikut
[https://vuejs.id/video-tutorial/q1dKK7euDak.html](https://vuejs.id/video-tutorial/q1dKK7euDak.html)

Dokumentasi mengenai `axios` bisa ditemukan di tautan berikut
[https://github.com/axios/axios](https://github.com/axios/axios)

### 3.5.2 Request di Vuex

Pada dasarnya, kita bisa saja meletakkan dimanapun proses *request* ke Rest API ini didalam projek kita. Kita bisa secara langsung memanggil sebuah *endpoint* dari komponen atau bahkan dari tempat lain. Namun pada kali ini, kita akan bahas salah satu *best practice* yang sering dilakukan oleh banyak pengembang, yakni meletakkan *request* ini pada `store` di `vuex`, lebih khususnya pada sebuah `actions`.

Hal ini dilakukan untuk melakukan pemusatan logika yang kita gunakan dalam melakukan request dan pengolahan data setelahnya, sehingga darimanapun kita akan memanggil `actions` tersebut, kita akan mendapatkan jenis balikan yang sama antara satu komponen dengan komponen lain.

Berikut adalah contoh bagaimana kita meletakan *request* ke Rest API didalam `vuex`:


```javascript
const API_URL = 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Fwwwid'

const store = new Vuex.Store({
  state: {
    articles: []
  },
  getters: {
    getArticles: state => {
      return state.articles
    }
  },
  mutations: {
    setArticles (state, value) {
      state.articles = value
    }
  },
  actions: {
    doGetArticles ({ commit }) {
      commit('setArticles', [])

      fetch(API_URL)
        .then(resp => resp.json())
        .then(data => {
          commit('setArticles', data.items)
        })
    }
  }
})
```

Dengan membuat `store` seperti kode diatas kita akan mudah untuk menggunakan di komponen manapun di dalam projek kita, seperti contoh kode berikut:

```html
<template>
  <ul>
    <li v-for="(article, index) in getArticles" :key="article.guid">
      {{ index + 1 }} - {{ article.title }} - ({{ article.author }})
    </li>
  </ul>
</template>

<script>
import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters([
      'getArticles'
    ])
  },
  methods: {
    ...mapActions([
      'doGetArticles'
    ]),
    onRequestData: function () {
      this.doGetArticles()
    }
  },
  mounted() {
    this.onRequestData();
  }
}
</script>
```

Pada projek nyata seringkali selain menyimpan hasil respons dari sebuah API *call* kita juga diharuskan melakukan beberapa interaksi tambahan semisal menampilkan dan menyembunyikan *log*, menampilkan pesan error dan interaksi lainnya. Untuk mengakomodir hal semacam ini, kita bisa juga mengubah sedikit fungsi di `actions` kita yang bisa menerima berbagai interaksi ini seperti contoh kode berikut:

```javascript
const API_URL = 'https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Fwwwid'

const store = new Vuex.Store({
  state: {
    articles: []
  },
  getters: {
    getArticles: state => {
      return state.articles
    }
  },
  mutations: {
    setArticles (state, value) {
      state.articles = value
    }
  },
  actions: {
    doGetArticles ({ commit }, {onSuccess, onError}) {
      commit('setArticles', [])

      fetch(API_URL)
        .then(resp => resp.json())
        .then(data => {
          commit('setArticles', data.items)
          // fired onSuccess
          onSuccess(data)
        })
        .catch(error => {
          // fired onError
          onError(daerrorta)
        });
    }
  }
})
```

Dengan perubahan kode seperti diatas, maka kita bisa mengubah kode di komponen kita juga agar bisa melempar interaksi ketika sukses dan gagal melakukan *reques* seperti berikut:

```html
<template>
  <ul v-if="!isLoading">
    <li v-for="(article, index) in getArticles" :key="article.guid">
      {{ index + 1 }} - {{ article.title }} - ({{ article.author }})
    </li>
    <li v-if="errMessage !== ''">
      {{ errMessage }}
    </li>
  </ul>
</template>

<script>
import { mapGetters, mapActions } from 'vuex'

export default {
  data () {
    return {
      isLoading: false,
      errMessage: ''
    }
  },
  computed: {
    ...mapGetters([
      'getArticles'
    ])
  },
  methods: {
    ...mapActions([
      'doGetArticles'
    ]),
    onRequestData: function () {
      this.isLoading = true
      this.errMessage = ''

      this.doGetArticles({
        onSucess: () => {
          this.isLoading = false
        },
        onError: (err) => {
          this.isLoading = false
          this.errMessage = err
        }
      })
    }
  },
  mounted() {
    this.onRequestData();
  }
}
</script>
```

## 3.7 Deployment Vue.js

Proses deployment bisa jadi pengetahuan yang vital, apalagi bila kita ingin projek yang kita buat bisa live dan mudah diakses oleh orang lain. Meskipun Vue.js bisa dideploy bersamaan dengan backend seperti Laravel yang secara official mendukungnya, namun pada tulisan kali ini kita hanya akan membahas deployment yang spesifik pada Vue.js saja. 

Sebelum memulai belajar deployment, pastikan bila kita memutuskan menggunakan `Vue-Router` maka kita telah set dalam mode `hash` agar tidak perlu lagi melakukan setup terhadap web server yang memang kita tidak miliki ketika menggunakan layanan static hosting.

Berikut beberapa hal yang akan kita bahas berkaitan dengan deployment dari Vue.js:


## 3.7.1 Deployment ke GitHub Pages

[GitHub Pages](https://pages.github.com/) (GH Pages) merupakan *free static hosting* yang disediakan oleh GitHub. Dengan populernya penggunaan GitHub, maka penggunaan GH Pages pun saya rasa merupakan pilihan terbaik saat ini bila ingin melakukan deployment sebuah projek Vue.js. Selain gratis, GH Pages juga secara default sudah didukung oleh semua repo yang ada disana, tidak terbatas untuk berapapun repo public yang kita miliki. Saya sendiri sangat benyak menggunakan GH Pages terutama untuk membuat halaman demo dari library ataupun projek Vue.js yang saya buat.

Sebelum kita membahas lebih lanjut, ada baiknya kita pelajari bagaimana cara membuat sebuah halaman statis menggunakan GH Pages terlebih dahulu.

Untuk mempunyai GH Pages, kita cukup mempunyai akun Github dan membuat repository dengan nama `{username}.github.io`. Secara otomatis kita akan dibuatkan website dengan alamat `https://{username}.github.io`, dan setiap kali kita melakukan perubahan pada branch `master` maka website yang dibuatkan tersebut juga akan langsung berubah meskipun pada beberapa kasus kita diharuskan menunggu paling tidak 10 menit sebelum tersebut bisa kita lihat dikarenakan cache yang diterapkan oleh Github.

Lain lagi bila kita ingin membuat sebuah halaman demo atau website bagi repository kita yang lain selain dari repository `{username}.github.io`. Untuk membuat halaman demo kita cukup membuat branch baru dengan nama `gh-pages` dan otomatis Github akan membuatkan halaman demo dengan alamat `https://{username}.github.io/{nama_repository}`. Yang perlu kita cermati bahwa github meletakan halaman tersebut sebagai folder dan bukan subdomain, yang mana ini seringkali membuat halaman SPA seperti Vue.js ini mengalami kesalahan dalam mengenali *root* dari domain halaman demo yang dibuat.

Karena Github menggunakan folder untuk halaman demo kita, maka salah satu cara agar halaman demo kita bisa berjalan dengan baik ketika di deploy adalah dengan mengganti settingan `publicPath` yang ada di build configuration berbasiskan webpack. Bila tadinya kita melakukan set ke `/` maka kita ganti dengan nama repository yang akan di deploy, seperti: `/{nama_repository}`. Bila terasa susah untuk mengganti settingan ini, saya sendiri seringkali cukup dengan menyalin dari kode lama dan dibuatkan file baru dengan `publicPath` yang berbeda. Namun bila kita belajar dari repository [Explore-Github](https://github.com/mazipan/explore-github/blob/master/webpack.config.js#L25) yang saya telah buat sebelumnya, saya justru lebih memilih menyamakan lingkungan development dengan lingkungan ketika nanti dideploy di GH Pages, sehingga saya bukannya akan membuka `http://localhost:3000` namun akan membuka `http://localhost:3000/explore-github` dan tidak perlu lagi mengganti `publicPath` saat proses build untuk file yang akan dideploy.

Setelah selesai melakukan setup `publicPath`, berikutnya tugas kita adalah memindahkan file hasil build yang biasanya berada di folder `/dist` ke dalam root folder di branch `gh-pages` agar terdeteksi sebagai halaman demo. Untuk mengerjakan hal ini, saya biasa menggunakan *third party plugin* yang membantu memindahkan folder ini yakni [gh-pages](https://www.npmjs.com/package/gh-pages). Kita bisa menambahkan ini ke dalam `package.json` terlebih dahulu dengan perintah:

```bash
npm i gh-pages --save-dev
#atau
yarn add gh-pages -D
```

Berikutnya kita akan membuat file baru, misal kita beri nama `deploy.js` yang berisi script untuk memidahkan folder hasil build kita, seperti contoh kode berikut ini:

```js
const ghpages = require('gh-pages');

const BRANCH_DEST = 'gh-pages';
const FOLDER_TO_MOVE = 'dist';
const TODAY = new Date().toLocaleString();

console.log(`start publishing to ${BRANCH_DEST}`);

ghpages.publish(FOLDER_TO_MOVE, {
  branch: BRANCH_DEST,
  message: `Deployed to ${BRANCH_DEST} in ${TODAY}`
}, () => {
  console.log(`done publishing to ${BRANCH_DEST}`);
});
```

Untuk menjalankan file ini kita cukup menggunakan perintah:

```bash
node ./deploy.js
```

Kita bisa juga menyelipkan perintah kedalam `package.json` kita seperti berikut:

```json
{
  "name": "deploy-vue",
  "scripts": {
    "deploy": "node ./deploy.js"
  },
  "devDependencies": {
    "gh-pages": "1.1.0",
}
```

Dengan ini, setiap kali kita ingin melakukan deployment kita cukup menjalankan perintah:

```bash
npm run deploy
```


## 3.7.2 Deployment ke Netlify

[Netlify](https://www.netlify.com/) adalah salah satu vendor yang meyediakan free static hosting pula untuk kita coba pada projek kita. Kita bisa juga menyetel otomasi di Netlify seperti pada GH Pages agar setiap kali kita melakukan perubahan pada suatu branch maka website yang kita deploy diperbarui pula.

Berbeda dengan GH Pages yang membuatkan folder, Netlify memilih untuk membuatkan subdomain bagi project yang ingin di deploy di paltform mereka. Hal ini justru memudahkan bagi kita, karena tidak perlu mengganti `publicPath` pada konfigurasi webpack.

Langkah untuk melakukan deployment pun sangat mudah di Netlify ini, kita cukup membuat akun dan login ke websitenya, saya sendiri lebih senang langsung login dengan menggunakan akun Github sehingga lebih mudah untuk integrasi.

Setelah masuk ke dashboard Netlify, kita bisa memilih membuat website baru (*Create New Site*), dan memilih untuk langsung diintegrasikan dengan Github sehingga akan muncul daftar repository publik yang ada di akun Github kita. Kita bisa memilih salah satu repository tersebut, kemudian memilih branch yang akan jadi *production branch*. Saya sendiri memilih menggunakan branch `gh-pages` pada contoh kali ini, kalian bisa terlebih dahulu membjuat branch baru di repository tersebut.

![Membuat website di Netlify](https://mazipan.github.io/wp-contents/images/netlify-choose-branch.png)

Branch yang dipilih ini nantinya akan jadi trigger untuk melakukan deployment terbaru bila terjadi perubahan pada salah satu file yang ada disana. Nantinya kita juga bisa memilih manual atau mengubah branch mana yang akan kita jadikan production branch bagi halaman di Netlify kita.

![Setup CD di Netlify](https://mazipan.github.io/wp-contents/images/netlify-setup-cd.png)

Netlify akan membuatkan subdomain secara acak bagi halaman kita, namun kita juga bisa mengganti dengan nama yang lebih mudah dibaca oleh pengguna kita di dashboard Netlify kita.

Karena biasanya Netlify tidak menggunakan branch `master` untuk melakukan deployment, maka seperti pada GH Pages kitapun akan membutuhkan bantuan library `gh-pages` untuk memindahkan file hasil build kita ke branch baru yang akan di deploy. Silahkan baca kembali topik sebelumnya mengenai deployment di GH Pages .


## 3.7.3 Deployment ke Firebase Hosting

[Firebase Hosting](https://firebase.google.com/) merupakan free hosting yang disediakan oleh Google Firebase. Firebase Hosting sudah mendukung HTTP/2 sehingga sangat baik untuk kita yang mendambakan performa yang lebih baik bagi aplikasi kita.

Langkah pertama kita harus masuk ke console firebase di [https://console.firebase.google.com/](https://console.firebase.google.com/) dan membuat projek baru untuk deployment kita nantinya.

![Membuat projek baru di Firebase](https://cdn-images-1.medium.com/max/1600/1*8XuXhbgUjir4k03y8wXhJw.png)

Setelah membuat projek baru, kita bisa memasang `Firebase CLI` di CMD kita denga perintah:

```bash
npm i -g firebase-tools
```

Setelahnya, kita diharuskan melakukan login ke firebase lewat CMD dengan perintah:

```bash
firebase login
```

![Contoh perintah `firebase login`](https://cdn.scotch.io/1/KYhZViFDQ7uEQl7ILXi7_T49yq47.png)

Setelah berhasil login, kita akan bisa membuat berbagai file yang dibutuhkan oleh firebase iuntuk melakukan deployment, kita bisa memanfaatkan perintah berikut untuk mempermudah:

```bash
firebase init
```

![Contoh perintah `firebase init`](https://cdn.scotch.io/1/mYIGynqTbu36cxodzXMB_vt3AyaP.png)

Dan untuk melakukan deployment, kita cukup mengetikkan perintahberikut di CMD:

```bash
firebase deploy
```

![Contoh perintah `firebase deploy`](https://cdn.scotch.io/1/WWhRstHPQdGSJp4T9XWK_oR2kynR.png)

Firebase bisa di konfigurasi dengan lebih *custom* dengan mengubah nilai yang terdapat pada `firebase.json`. Sayangnya, sampai pada bagian ini kita belum bisa mengotomasi deployment ke firebase setiap kali melakukan perubahan file di Github repository kita. Deployment dilakukan manual oleh CMD lokal kita, karena membutuhkan login untuk autentikasi agar firebase bisa mengenali dan mengidentifikasi akses kita ke projek di firebase. Kita bisa memanfaatkan third party CI/CD seperti Travis CI untuk melakukan otomasi deployment ke firebase.

## 3.7.4 Otomasi Deployment Firebase dengan Travis-CI

[Travis-CI](https://travis-ci.org/) merupakan layanan penyedia *continuous integration/continuous delivery* (CI/CD) yang juga memberikan akses gratis bagi pemegang repository open-source projek seperti Github. Kita bisa memanfaatkan Travis-CI untuk menjalankan auto build, auto test dan auto deployment ketika terjadi perubahan pada file di branch `master` misalnya.

Kita bisa memulai dengan login di website [https://travis-ci.org/](https://travis-ci.org/) dan masuk ke dashboard Travis-CI untuk kemudian menambahkan repository dari Github kita yang ingin kita deploy menggunakan layanan dari Travis-CI ini.

Untuk bisa melakukan deployment di Travis-CI, kita perliu menambahkan file `.travis.yml` yang sisinya bisa kita sesuaikan dengan cara kita melakukan deployment file kita, berikut contoh dari file konfigurasi yang saya gunakan untuk melakukan deployment ke Firebase:

```yml
language: node_js
node_js: '8.10.0'
branches:
   only:
      - master
before_script:
   - npm install -g --silent firebase-tools
script:
  - npm run build
after_success:
  - firebase deploy --token $FIREBASE_TOKEN
```

Bisa dilihat pada file konfigurasi diatas, bahwa kita memasang `Firebase CLI` ke *Virtual Machine* (VM) yang diberikan oleh Travis-CI. `Firebase CLI` kita pasang pada state `before_script` yang artinya akan diajalankan terlebih dahulu sebelum menjalankan script yang kita tuliskan untuk melakukan build file production kita.

Setelah berhasil memasang `Firebase CLI`, kita bisa menambahkan script yang kita butuhkan untuk build file pada bagian `script` pada file `.travis.yml`. Dan terkahir kita melakukan deployment ke firebase dengan menyertakan token yang kita dapatkan dari Firebase CLI kita.

Untuk mendapatkan token tersebut kita bisa menggunakan perintah:

```bash
firebase login:ci
```

Pada dasarnya, Travis-CI menganjurkan kita untuk melakuka enkripsi pada token yang kita dapatkan sebelum digunakan. Tapi karena pada saat itu saya mendapatkan error yang tidak terduga saat melakukan enkripsi dengan travis maka saya putuskan untuk meletakan token di Environment Variable yang bisa kita akses dari menu setting di dashboard Travis CI pada repository kita.

![Set FIREBASE_TOKEN di environment variable](https://mazipan.github.io/wp-contents/images/travis-firebase-token.png)

Setelah meletakan token di bisa melakukan deployment dengan perintah berikut yang kita letakan pada state `after_success` pada file `.travis.yml`:

```bash
firebase deploy --token $FIREBASE_TOKEN
```

Selengkapnya bisa dibaca di tautan berikut:
[https://docs.travis-ci.com/user/deployment/firebase/](https://docs.travis-ci.com/user/deployment/firebase/)


## 3.7.5 Konfigurasi Web Server

Seperti kita sudah singgung di awal, bahwa *Vue Router* mendukung dua mode yakni history dan hash. Ketika kita memutuskan menggunakan mode *hash*, mugnkin kita tidak perlu menambahkan konfigurasi apapun. Berbeda jika kita memutuskan menggunakan mode *history* dimana menambahkan sedikit kofigurasi pada webserver kita seperti NGINX ataupun Apache. Konfigurasi ini ditujukan agar mengembalikan semua url yang kita mapping di frontend agar selalu mengarah ke index.html yang sama, sehingga ketika halaman dimuat ulang kita tidak lagi mengalami respons 404.

Berikut konfigurasi sederhana dari webserver yang harus ditambahkan:


### 3.7.5.1 NGINX

Tambahkan konfigurasi berikut di file `nginx.conf` di folder NGINX kalian:

```bash
location / {
  try_files $uri $uri/ /index.html;
}
```


### 3.7.5.2 Apache

Tambahkan konfigurasi berikut di ]`apache.conf` kalian:

```bash
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /index.html [L]
</IfModule>
```


### 3.7.5.3 Firebase Hosting

Tambahkan konfigurasi berikut pada `firebase.json` kalian:

```json
{
  "hosting": {
    "public": "dist",
    "rewrites": [
      {
        "source": "**",
        "destination": "/index.html"
      }
    ]
  }
}
```

-------

> Objectif: Setelah membaca bagian ini diharapkan pembaca sudah bisa melakukan konfigurasi untuk menyiapkan dan praktek langsung serta mencoba berbagai kode dasar dari Vue yang akan lebih banyak lagi kita tampilkan pada bagian berikutnya.